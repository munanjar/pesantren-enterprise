<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Anggota_model extends CI_Model {
    var $_table = 'anggota';
    var $_aktivasi = 'aktivasi';
    
    function simpan($data){
        if(!empty($data['id'])){
            $hasil = $this->db->update($this->_table, $data, array('id' => $data['id']));
        }else{
            $hasil = $this->db->insert($this->_table, $data);
            $id = $this->db->insert_id();
            $initial = date('ym');
            $this->db->update($this->_table, array('userid' => sprintf($initial . '%06d', $id)), array('id' => $id));
            
            $kode_unik = rand(100, 10000);
            $aktivasi = array(
                    'anggota_id' => $id,
                    'kode_unik' => $kode_unik,
                    'nominal' => 1000000,
                    'status' => 'DAFTAR'
                );
            $this->db->insert($this->_aktivasi, $aktivasi);
            
        }
        return $hasil;
    }
    
    function get_anggota($id){
        return $this->db->get_where($this->_table, array('id' => $id))->row_array();
    }
    
    function get_data(){
        return $this->db->get($this->_table)->result_array();
    }
    
    function get_data_edit($id){
        if(!empty($id)){
            $this->db->where('id', $id);
        }
        return $this->db->get($this->_table)->row_array();
    }
    
    function search_kyai($keyword){
        $this->db->select('CONCAT(userid," - ", nama_lengkap) as nama, userid, id');
        $this->db->where('CONCAT(userid, nama_lengkap) LIKE ', '%'.$keyword.'%');
        $this->db->where('kelompok', 'KYAI');
        return $this->db->get($this->_table)->result_array();
    }
    
    function search_ustadz($keyword){
        $this->db->select('CONCAT(userid," - ", nama_lengkap) as nama, userid, id');
        $this->db->where('CONCAT(userid, nama_lengkap) LIKE ', '%'.$keyword.'%');
        $this->db->where('kelompok', 'USTADZ');
        return $this->db->get($this->_table)->result_array();
    }
    
    function delete($id){
        $data = array('status' => 'HAPUS');
        return $this->db->update($this->_table, $data, array('id' => $id));
    }
    
    function get_data_prov($id){
        if(!empty($id)){
            $this->db->where('provinsi_id', $id);
        }
        $hasil = $this->db->get('wilayah_provinsi')->row_array();
        $result = array(
            'provinsi' => !empty($hasil['nama']) ? $hasil['nama'] : ''
        );
        
        return $result;
        
    }
}
?>