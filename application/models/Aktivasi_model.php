<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Aktivasi_model extends CI_Model {
    var $_table = 'aktivasi';
    
    function get_all_data(){
        $this->db->select('a.id, a.kode_unik, a.nominal, a.anggota_id, b.userid, b.nama_lengkap, b.alamat, b.parent_userid, b.telp');
        $this->db->from('aktivasi AS a');
        $this->db->join('anggota AS b ', 'b.id = a.anggota_id');
        $this->db->where('a.status', 'DAFTAR');
        return $this->db->get()->result_array();
    }
    
    function get_aktivasi($aktivasi_id){
        return $this->db->get_where($this->_table, array('id' => $aktivasi_id))->row_array();
    }
    
    function aktivasi($data){
        try {
            // 1. Update tabel aktivasi
            $aktivasi = $this->get_aktivasi($data['aktivasi_id']);
            $anggota = $this->db->get_where('anggota', array('id' => $aktivasi['anggota_id']))->row_array();
            
            $item_aktivasi = array(
                'kode_transaksi' => '',
                'status' => 'BAYAR',
                'keterangan' => $data['keterangan'],
                'cara_bayar' => $data['cara_bayar'],
                'nominal_bayar' => $data['nominal_bayar'],
                'bank_bayar' => $data['bank_transfer'],
                'bukti_bayar' => $data['bukti_pembayaran'],
                'tanggal_bayar' => date('Y-m-d', strtotime($data['tgl_bayar'])),
                'modified_date' => date('Y-m-d H:i:s'),
                'modified_by' => $this->session->userdata('username')
            );
            $this->db->update('aktivasi', $item_aktivasi, array('id' => $data['aktivasi_id']));
            
            // 2. Update tabel anggota
            $item_anggota = array(
                'status' => 'AKTIF',
                'modified_date' => date('Y-m-d H:i:s'),
                'modified_by' => $this->session->userdata('username')
            );
            $this->db->update('anggota', $item_anggota, array('id' => $aktivasi['anggota_id']));

            // 3. insert tabel angsuran
            $item_angsuran = array(
                'anggota_id' => $aktivasi['anggota_id'],
                'tanggal' => date('Y-m-d', strtotime($data['tgl_bayar'])),
                'kode_transaksi' => $item_aktivasi['kode_transaksi'],
                'keterangan' => 'AKTIVASI PENDAFTARAN',
                'debet' => '',
                'kredit' => 26000000,
                'saldo' => -26000000,
                'status' => '',
                'angsuran_ke' => 0,
                'created_date' => date('Y-m-d H:i:s'),
                'created_by' => $this->session->userdata('username')
            );
            $this->db->insert('angsuran', $item_angsuran);
            
            // 4. insert tabel jurnal harian
            $item_jurnal_harian = array(
                'ref_akun_id' => '',
                'ref_akun_nama' => '',
                'tanggal' => date('Y-m-d H:i:s'),
                'kode_transaksi' => $aktivasi['kode_transaksi'],
                'group_transaksi' => 'Registrasi',
                'keterangan' => 'Pendaftaran Anggota '.$anggota['nama_lengkap'],
                'debet' => $data['nominal_bayar'],
                'kredit' => '',
                'saldo' => '',
                'status' => '',
                'created_date' => date('Y-m-d H:i:s'),
                'created_by' => $this->session->userdata('username'),
                'table_id' => $aktivasi['id'],
                'table_name' => 'aktivasi'
            );
            $this->db->insert('jurnal_harian', $item_jurnal_harian);
            
            // 5. insert tabel jurnal anggota
            

            // 6. insert tabel fee anggota
            switch ($anggota['kelompok']) {
                case 'USTADZ':
                    $kyai = $this->db->get_where('anggota', array('id' => $anggota['parent_id']))->row_array();
                    if(!empty($kyai)){
                        $item_fee_anggota = array(
                            'aktivasi_id' => $aktivasi['id'],
                            'from_anggota_id' => $anggota['id'],
                            'to_anggota_id' => $kyai['id'],
                            'tanggal_bayar' => '',
                            'cara_bayar' => '',
                            'nominal_bayar' => 400000,
                            'bank_bayar' => '',
                            'no_rek_bayar' => '',
                            'nama_rek_bayar' => '',
                            'keterangan' => 'Komisi Pendaftaran a/n '.$anggota['nama_lengkap'],
                            'status' => 'PROSES',
                            'modified_date' => date('Y-m-d H:i:s'),
                            'modified_by' => $this->session->userdata('username')
                        );
                        $this->db->insert('fee_anggota', $item_fee_anggota);
                    }
                    break;
                case 'JAMAAH':
                    // 1. Fee ke Ustadz
                    $ustadz = $this->db->get_where('anggota', array('id' => $anggota['parent_id']))->row_array();
                    if(!empty($ustadz)){
                        $item_fee_anggota = array(
                            'aktivasi_id' => $aktivasi['id'],
                            'from_anggota_id' => $anggota['id'],
                            'to_anggota_id' => $ustadz['id'],
                            'tanggal_bayar' => '',
                            'cara_bayar' => '',
                            'nominal_bayar' => 400000,
                            'bank_bayar' => '',
                            'no_rek_bayar' => '',
                            'nama_rek_bayar' => '',
                            'keterangan' => 'Komisi Pendaftaran a/n '.$anggota['nama_lengkap'],
                            'status' => 'PROSES',
                            'modified_date' => date('Y-m-d H:i:s'),
                            'modified_by' => $this->session->userdata('username')
                        );
                        $this->db->insert('fee_anggota', $item_fee_anggota);
                        
                        // 2. Fee ke Kyai
                        $kyai = $this->db->get_where('anggota', array('id' => $ustadz['parent_id']))->row_array();
                        if(!empty($kyai)){
                            $item_fee_anggota = array(
                                'aktivasi_id' => $aktivasi['id'],
                                'from_anggota_id' => $ustadz['id'],
                                'to_anggota_id' => $kyai['id'],
                                'tanggal_bayar' => '',
                                'cara_bayar' => '',
                                'nominal_bayar' => 300000,
                                'bank_bayar' => '',
                                'no_rek_bayar' => '',
                                'nama_rek_bayar' => '',
                                'keterangan' => 'Komisi 2 Pendaftaran a/n '.$anggota['nama_lengkap'],
                                'status' => 'PROSES',
                                'modified_date' => date('Y-m-d H:i:s'),
                                'modified_by' => $this->session->userdata('username')
                            );
                            $this->db->insert('fee_anggota', $item_fee_anggota);
                        }
                    }
                    break;
                default:
                    break;
            }
            return TRUE;
        } catch (Exception $exc) {
            // echo $exc->getTraceAsString();
            return FALSE;
        }
    }
    
    function get_lap_registrasi($awal, $akhir){
        $this->db->select('a.id, a.nominal_bayar, a.created_date as tanggal_daftar, a.tanggal_bayar as tanggal_aktif, a.cara_bayar, a.bank_bayar, a.kode_transaksi, b.parent_id, b.userid, b.nama_lengkap, b.status, b.kelompok');
        $this->db->from('aktivasi AS a');
        $this->db->join('anggota AS b', 'b.id = a.anggota_id');
        if(!empty($awal) && !empty($akhir)){
            $this->db->where('DATE_FORMAT(b.tanggal,"%Y-%m-%d") >=', $awal);
            $this->db->where('DATE_FORMAT(b.tanggal,"%Y-%m-%d") <=', $akhir);
        }
        return $this->db->get()->result_array();
    }
}
?>