<?php

/*
  Fungsi : Model untuk modul admin
 */

class User_model extends CI_Model {

    /**
     * Constructor
     */
    public function __construct() {
        // Constructor's functionality here, if you have any.
    }

    function User_model() {
        parent::__construct();
    }

    /* Insialiasi nama table */

    var $table = 'user';

    /* -----------------------Home Model--------------------------- */

    
    /* Get data */

    function cek_user($user, $pass) {
        $this->db->select('*');
        $this->db->where('username', $user);
        $this->db->where('password', md5($pass));
        return $this->db->get($this->table)->row_array();
    }
    
    function get_data_anggota($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $hasil = $this->db->get('anggota')->row_array();
       
        $result = array(
            'email' => !empty($hasil['email']) ? $hasil['email'] : ''
        );
        //$this->myDebug($result);
        return $result;
    }

}

/*Location : /application/model/Home_model*/