<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Administrator_model extends CI_Model {
    var $_pengguna = 'user';
    function get_laporan_aktivitas($awal, $akhir){
        if(!empty($awal)){
            
        }
        if(!empty($akhir)){
            
        }
        $this->db->from('log_aktivitas');
        $this->db->order_by('id DESC');
        return $this->db->get()->result_array();
    }
    
    function get_aktivitas($log_aktivitas_id){
        return $this->db->get_where('log_aktivitas', array('id' => $log_aktivitas_id))->row_array();
    }
    
    function get_laporan_error($awal, $akhir){
        if(!empty($awal)){
            
        }
        if(!empty($akhir)){
            
        }
        $this->db->from('log_error');
        $this->db->order_by('id DESC');
        return $this->db->get()->result_array();
    }
    
    function get_error($log_error_id){
        return $this->db->get_where('log_error', array('id' => $log_error_id))->row_array();
    }
    
    function get_kelompok($kelompok_id){
        return $this->db->get_where('kelompok', array('id' => $kelompok_id))->row_array();
    }
    
    function get_list_menu($kelompok_id){
        $this->db->from('menu_user');
        return $this->db->get()->result_array();
    }
    
    function get_data_user($param){
        $this->db->select('a.*, b.nama as kelompok');
        $this->db->join('kelompok as b','b.id = a.kelompok_id');
        $this->db->from('user as a');
        if(!empty($param)){
            $this->db->where('a.id', $param);
        }
        return $this->db->get()->result_array();
    }
    
    function get_data_kelompok(){
        return $this->db->get('kelompok');
    }
    
    function simpan($data){
        if(!empty($data['id'])){
            $hasil = $this->db->update($this->_pengguna, $data, array('id' => $data['id']));
        }else{
            $hasil = $this->db->insert($this->_pengguna, $data);
        }
        return $hasil;
    }
    
}
?>