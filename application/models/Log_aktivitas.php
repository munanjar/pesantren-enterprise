<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Log_aktivitas extends CI_Model {
    var $table = 'log_aktivitas';

    public function __construct() {
        parent::__construct();
    }

    function save_aktivitas($arr_data){
        return $this->db->insert($this->table, $arr_data);
    }
    
}

?>
