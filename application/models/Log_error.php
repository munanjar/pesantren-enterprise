<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Log_error extends CI_Model {
    var $table = 'log_error';

    public function __construct() {
        parent::__construct();
    }

    function save_error($arr_data){
        return $this->db->insert($this->table, $arr_data);
    }
    
}

?>