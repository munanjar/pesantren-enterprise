<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->alert->check_login();
        $this->load->model('Anggota_model');
        $this->load->model('Angsuran_model');
        $this->load->model('Pembayaran_model');
        $this->load->model('Aktivasi_model');
    }

    function fee_konfirmasi() {
        $data = $this->input->post();
        if(!empty($data)){
            try {
                $data['nominal_bayar'] = $this->get_number_from_string($data['nominal_bayar']);
                
                if(!$this->Pembayaran_model->simpan_fee_anggota($data)){
                    throw new Exception('Ada kesalahan ketika simpan pembayaran fee anggota');
                }
                $this->simpan_aktivitas('Konfirmasi Pembayaran Fee Anggota', $data);
                $this->session->set_flashdata('message_ok', 'Data Fee Anggota Telah Tersimpan');
            } catch (Exception $exc) {
                $msg = $exc->getMessage();
                $this->simpan_error('Konfirmasi Fee Anggota', $data, $exc);
                $this->session->set_flashdata('message_err', $msg);
            }
        }
        $this->data['content'] = $this->load->view('pembayaran/fee_konfirmasi', $this->data, TRUE);
        $this->load->view('layout/layout_utama', $this->data);
    }
    
    function ajax_fee_anggota(){
        $list_data = $this->Pembayaran_model->get_aktif_fee_anggota();
        $data = array();
        $no = 1;
        foreach ($list_data as $item) {
            $row = array();
            $row[] = $no;
            $row[] = $item['tanggal_bayar'];
            $row[] = $item['userid'].' - '.$item['nama_lengkap'];
            $row[] = $item['keterangan'];
            $row[] = number_format($item['nominal_bayar'],0,',','.');
            $row[] = $item['status'];
            $row[] = '<button class="btn btn-warning" onclick="konfirmasi('.$item['id'].')"> Konfirmasi </button>';
            $data[] = $row;
            $no++;
        }
        $output = array(
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    function get_data_fee($fee_anggota_id){
        $fee_anggota = $this->Pembayaran_model->get_fee_anggota($fee_anggota_id);
        if(!empty($fee_anggota)){
            $aktivasi = $this->Aktivasi_model->get_aktivasi($fee_anggota['aktivasi_id']);
            $to_anggota = $this->Anggota_model->get_anggota($fee_anggota['to_anggota_id']);
            $hasil = array(
                'fee_anggota_id' => $fee_anggota_id,
                'to_anggota_nama' => $to_anggota['userid'].' - '.$to_anggota['nama_lengkap'],
                'keterangan' => $fee_anggota['keterangan'],
                'nominal_bayar' => number_format($fee_anggota['nominal_bayar'],0,',','.'),
                'bank_transfer' => $to_anggota['bank'],
                'no_rek_bayar' => $to_anggota['no_rek'],
                'nama_rek_bayar' => $to_anggota['nama_rek'],
            );
        }else{
            $hasil = array(
                'fee_anggota_id' => '',
                'to_anggota_nama' => '',
                'keterangan' => '',
                'nominal_bayar' => '',
                'bank_transfer' => '',
                'no_rek_bayar' => '',
                'nama_rek_bayar' => '',
            );            
        }
        echo json_encode($hasil);
    }
    
    function angsuran_add() {
        $data = $this->input->post();
        if (!empty($data)) {
            try {
                $data['nominal_bayar'] = $this->get_number_from_string($data['nominal_bayar']);
                
                if(!$this->Pembayaran_model->bayar_angsuran($data)){
                    throw new Exception('Ada kesalahan ketika simpan data angsuran anggota');
                }
                
                $this->simpan_aktivitas('Angsuran Anggota', $data);
                $this->session->set_flashdata('message_ok', 'Data Angsuran Anggota Berhasil Disimpan');
            } catch (Exception $exc) {
                $msg = $exc->getMessage();
                $this->simpan_error('Angsuran Anggota', $data, $exc);
                $this->session->set_flashdata('message_err', $msg);
            }
        }
        $this->data['content'] = $this->load->view('pembayaran/angsuran_add', $this->data, TRUE);
        $this->load->view('layout/layout_utama', $this->data);
    }
    
    function ajax_data_angsuran(){
        $list_data = $this->Pembayaran_model->get_data_angsuran();
        $data = array();
        $no = 1;
        foreach ($list_data as $item) {
            $total_angsuran = $this->Angsuran_model->get_total_angsuran($item['id']);
            $angsuran_anggota = $this->Angsuran_model->get_data_angsuran($item['id']);
            $row = array();
            $row[] = $no;
            $row[] = $item['userid'].' <br> '.$item['nama_lengkap'];
            $row[] = $item['kelompok'];
            $row[] = number_format($total_angsuran,0,',','.');
            $row[] = number_format($total_angsuran,0,',','.');
            $row[] = number_format($total_angsuran,0,',','.');
            $row[] = number_format($total_angsuran,0,',','.');
            $row[] = '<button class="btn btn-warning" onclick="angsuran('.$item['id'].')"> Angsuran </button>';
            $data[] = $row;
            $no++;
        }
        $output = array(
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    function get_data_anggota($anggota_id){
        $anggota = $this->Anggota_model->get_anggota($anggota_id);
        if(!empty($anggota)){
            $hasil = array(
                'anggota_id' => $anggota_id,
                'nama' => $anggota['userid'].' - '.$anggota['nama_lengkap'],
                'alamat' => $anggota['alamat'],
                // 'nominal_bayar' => number_format($anggota['nominal_bayar'],0,',','.'),
                'nominal_bayar' => '650.000',
                'bank_transfer' => $anggota['bank'],
                'no_rek_bayar' => $anggota['no_rek'],
                'nama_rek_bayar' => $anggota['nama_rek'],
            );
        }else{
            $hasil = array(
                'anggota_id' => '',
                'nama' => '',
                'alamat' => '',
                'nominal_bayar' => '',
                'bank_transfer' => '',
                'no_rek_bayar' => '',
                'nama_rek_bayar' => '',
            );            
        }
        echo json_encode($hasil);
    }
    
    function angsuran_konfirmasi() {
        $data = $this->input->post();
        if(!empty($data)){
            try {
                $data['nominal_bayar'] = $this->get_number_from_string($data['nominal_bayar']);
                
                if(!$this->Pembayaran_model->konfirmasi_angsuran($data)){
                    throw new Exception('Ada kesalahan ketika simpan konfirmasi pembayaran angsuran anggota');
                }
                $this->simpan_aktivitas('Konfirmasi Pembayaran Angsuran Anggota', $data);
                $this->session->set_flashdata('message_ok', 'Data Konfirmasi Pembayaran Angsuran Anggota Telah Tersimpan');
            } catch (Exception $exc) {
                $msg = $exc->getMessage();
                $this->simpan_error('Konfirmasi Pembayaran Angsuran Anggota', $data, $exc);
                $this->session->set_flashdata('message_err', $msg);
            }
        }
        $this->data['content'] = $this->load->view('pembayaran/angsuran_konfirmasi', $this->data, TRUE);
        $this->load->view('layout/layout_utama', $this->data);
    }
    
    function ajax_angsuran_bayar(){
        $list_data = $this->Pembayaran_model->get_aktif_angsuran_bayar();
        $data = array();
        $no = 1;
        foreach ($list_data as $item) {
            $row = array();
            $row[] = $no;
            $row[] = $item['tanggal'];
            $row[] = $item['userid'].' - '.$item['nama_lengkap'];
            $row[] = $item['keterangan'];
            $row[] = number_format($item['nominal_bayar'],0,',','.');
            $row[] = $item['status'];
            $row[] = '<button class="btn btn-warning" onclick="konfirmasi('.$item['id'].')"> Konfirmasi </button>';
            $data[] = $row;
            $no++;
        }
        $output = array(
            "data" => $data,
        );
        echo json_encode($output);
    }

    function get_data_angsuran_bayar($angsuran_bayar_id){
        $angsuran_bayar = $this->Pembayaran_model->get_angsuran_bayar($angsuran_bayar_id);
        if(!empty($angsuran_bayar)){
            $anggota = $this->Anggota_model->get_anggota($angsuran_bayar['anggota_id']);
            $hasil = array(
                'angsuran_bayar_id' => $angsuran_bayar_id,
                'nama' => $anggota['userid'].' - '.$anggota['nama_lengkap'],
                'keterangan' => $angsuran_bayar['keterangan'],
                'nominal_bayar' => number_format($angsuran_bayar['nominal_bayar'],0,',','.')
            );
        }else{
            $hasil = array(
                'angsuran_bayar_id' => '',
                'nama' => '',
                'keterangan' => '',
                'nominal_bayar' => ''
            );            
        }
        echo json_encode($hasil);
    }
    
    function lap_angsuran() {
        $this->data['content'] = $this->load->view('pembayaran/lap_angsuran', $this->data, TRUE);
        $this->load->view('layout/layout_utama', $this->data);
    }
    
    function ajax_lap_angsuran(){
        $awal = !empty($this->uri->segment(3)) ? $this->uri->segment(3) : '';
        $akhir = !empty($this->uri->segment(4)) ? $this->uri->segment(4) : '';
        
        $list_data = $this->Angsuran_model->get_lap_angsuran($awal, $akhir);
        $data = array();
        $no = 1;
        foreach ($list_data as $item) {
            
            $row = array();
            $row[] = $no;
            $row[] = $item['anggota_id'].' - '.$item['nama_lengkap'];
            $row[] = date('d/m/Y',strtotime($item['tanggal']));
            $row[] = $item['kode_transaksi'];
            $row[] = $item['angsuran_ke'];
            $row[] = $item['keterangan'];
            $row[] = '<b>Cara Bayar : </b>'.$item['cara_bayar'].
                     '<br><b>Nominal : </b>'.number_format($item['nominal_bayar']).
                     '<br><b>Bank : </b>'.$item['bank_bayar'];
            $row[] = $item['status'];
            $data[] = $row;
            $no++;
            
            
        }
        $output = array(
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    function export_lap_angsuran() {
        $post_data = $this->input->post(NULL, FALSE);
        
        $awal = !empty($post_data['awal']) ? $post_data['awal'] : '';
        $akhir = !empty($post_data['akhir']) ? $post_data['akhir'] : '';
        
        $hasil = $this->Angsuran_model->get_lap_angsuran($awal, $akhir);
        //$data = array();
        $no = 1;
        foreach ($hasil as $item) {
            $no++;
            
            $row = array();
            $item['id'] = $item['id'];
            $item['anggota'] = $item['anggota_id'].' - '.$item['nama_lengkap'];
            $item['tanggal'] = date('d/m/Y', strtotime($item['tanggal']));
            $item['kode_transaksi'] = $item['kode_transaksi'];
            $item['angsuran_ke'] = $item['angsuran_ke'];
            $item['keterangan'] = $item['keterangan'];
            $item['cara_bayar'] = $item['cara_bayar'];
            $item['nominal_bayar'] = number_format($item['nominal_bayar']);
            $item['bank_bayar'] = $item['bank_bayar'];
            $item['status'] = $item['status'];
            
            $data[] = $item;
        }
        $data['list'] = $data;
        
        $this->load->view('pembayaran/lap_angsuran_excel', ($data));
    }
    
    function lap_fee_anggota() {
        $this->data['content'] = $this->load->view('pembayaran/lap_fee_anggota', $this->data, TRUE);
        $this->load->view('layout/layout_utama', $this->data);
    }
    
    function ajax_lap_fee_anggota(){
        $awal = !empty($this->uri->segment(3)) ? $this->uri->segment(3) : '';
        $akhir = !empty($this->uri->segment(4)) ? $this->uri->segment(4) : '';
        
        $list_data = $this->Angsuran_model->get_lap_fee_anggota($awal, $akhir);
        
        $data = array();
        $no = 1;
        foreach ($list_data as $item) {
            
            $row = array();
            $row[] = $no;
            $row[] = $item['aktivasi_id'];
            $row[] = $item['kode_transaksi'];
            $row[] = '<b>Tgl Bayar : </b>'.date('d/m/Y',strtotime($item['tanggal_bayar'])).
                     '<br><b>Nominal : </b>'.number_format($item['nominal_bayar']).
                     '<br><b>Cara Bayar : </b>'.$item['cara_bayar'].
                     '<br><b>Bank : </b>'.$item['bank_bayar'].
                     '<br><b>No. Rek : </b>'.$item['no_rek_bayar'].
                    '<br><b>Nama Rek : </b>'.$item['nama_rek_bayar'];
            $row[] = $item['keterangan'];
            $row[] = $item['status'];
            $data[] = $row;
            $no++;
           
        }
        
        $output = array(
            "data" => $data,
        );
        //$this->myDebug($output);
        echo json_encode($output);
    }
    
    function export_lap_fee_anggota() {
        $post_data = $this->input->post(NULL, FALSE);
        
        $awal = !empty($post_data['awal']) ? $post_data['awal'] : '';
        $akhir = !empty($post_data['akhir']) ? $post_data['akhir'] : '';
        
        $hasil = $this->Angsuran_model->get_lap_fee_anggota($awal, $akhir);
        //$data = array();
        $no = 1;
        foreach ($hasil as $item) {
            $no++;
            
            $row = array();
            $item['aktivasi_id'] = $item['aktivasi_id'];
            $item['kode_transaksi'] = $item['kode_transaksi'];
            $item['tanggal_bayar'] = date('d/m/Y', strtotime($item['tanggal_bayar']));
            $item['cara_bayar'] = $item['cara_bayar'];
            $item['nominal_bayar'] = number_format($item['nominal_bayar']);
            $item['bank_bayar'] = $item['bank_bayar'];
            $item['no_rek_bayar'] = $item['no_rek_bayar'];
            $item['nama_rek_bayar'] = $item['nama_rek_bayar'];
            $item['keterangan'] = $item['keterangan'];
            $item['status'] = $item['status'];
            
            $data[] = $item;
        }
        $data['list'] = $data;
        
        $this->load->view('pembayaran/lap_fee_anggota_excel', ($data));
    }

    function jurnal_harian(){
        $this->data['content'] = $this->load->view('pembayaran/jurnal_harian', $this->data, TRUE);
        $this->load->view('layout/layout_utama', $this->data);
    }
    
    function ajax_jurnal_harian(){
        $awal = '';
        $akhir = '';
        $list_data = $this->Pembayaran_model->get_jurnal_harian($awal, $akhir);
        $data = array();
        $no = 1;
        $total = 0;
        foreach ($list_data as $item) {
            $row = array();
            $total = $total + $item['debet'] - $item['kredit'];
            $row[] = $no;
            $row[] = date('Y-m-d', strtotime($item['tanggal']));
            $row[] = $item['ref_akun_nama'];
            $row[] = $item['group_transaksi'];
            $row[] = $item['keterangan'];
            $row[] = number_format($item['debet'],0,',','.');
            $row[] = number_format($item['kredit'],0,',','.');
            $row[] = number_format($total,0,',','.');
            $data[] = $row;
            $no++;
        }
        $output = array(
            "data" => $data,
        );
        echo json_encode($output);
    }

}
?>