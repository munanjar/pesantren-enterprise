<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->alert->check_login();
    }

    function dashboard() {
        $this->data['total_anggota'] = 2017;
        $this->data['registrasi_bulan_berjalan'] = 453;
        $this->data['total_fee_anggota'] = 54125842;
        $this->data['total_angsuran'] = 1458726;
        $this->data['content'] = $this->load->view('home/dashboard', $this->data, TRUE);
        $this->load->view('layout/layout_utama', $this->data);
    }

}
?>