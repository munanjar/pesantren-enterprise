<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->alert->check_login();
        $this->load->model('Administrator_model');
    }

    function setting() {
        $this->data['content'] = $this->load->view('administrator/setting', $this->data, TRUE);
        $this->load->view('layout/layout_utama', $this->data);
    }

    // <editor-fold defaultstate="collapsed" desc="PENGGUNA">
    function pengguna() {
        $data = $this->input->post();
        if (!empty($data)) {
            try {
                if (!empty($data['id'])) {
                    $password = isset($data['reset']) ? md5($data['pass']) : $data['pass'];
                }else{
                    $password = md5($data['pass']);
                }
                $pengguna = array(
                    'id' => $data['id'],
                    'username' => $data['username'],
                    'password' => $password,
                    'nama' => $data['nama'],
                    'kelompok_id' => $data['kelompok'],
                    'is_active' => $data['status'],
                    'modified_date' => date('Y-m-d H:i:s'),
                    'modified_by' => $this->session->userdata('username')
                );

                if (!$this->Administrator_model->simpan($pengguna)) {
                    throw new Exception('Error ketika simpan data user');
                }
                $this->simpan_aktivitas('Simpan/Update data user', $data);
                $this->session->set_flashdata('message_ok', 'Data Pengguna Telah Tersimpan');
                redirect('administrator/pengguna');
            } catch (Exception $exc) {
                $msg = $exc->getMessage();
                $this->simpan_error('Simpan/Update Data pengguna ', $data, $exc);
                $this->session->set_flashdata('message_err', $msg);
            }
        }

        $this->data['content'] = $this->load->view('administrator/pengguna', $this->data, TRUE);
        $this->load->view('layout/layout_utama', $this->data);
    }

    // <editor-fold defaultstate="collapsed" desc=" Menu HAK AKSES ">
    function ajax_data_pengguna() {
        $list_data = $this->Administrator_model->get_data_user('');

        $data = array();
        $no = 1;
        foreach ($list_data as $item) {
            $row = array();
            $row[] = $no;
            $row[] = $item['username'];
            $row[] = $item['nama'];
            $row[] = $item['kelompok'];
            $row[] = $item['modified_by'];
            $row[] = $item['modified_date'];
            $row[] = ($item['is_active'] == '1') ? 'Aktif' : 'Non Aktif';
            $btnEdit = '<button class="btn btn-warning" onclick="edit_data(' . $item['id'] . ')"> Edit </button>';
            $btnDelete = '<button class="btn btn-danger" onclick="delete_data(' . $item['id'] . ')"> Delete </button>';
            $row[] = $btnEdit.' '.$btnDelete;
            $data[] = $row;
            $no++;
        }
        $output = array(
            "data" => $data,
        );
        //$this->myDebug($list_data);
        echo json_encode($output);
    }

    function edit($id) {
        $data = $this->Administrator_model->get_data_user($id);

        $hasil = array(
            'id' => $data[0]['id'],
            'username' => $data[0]['username'],
            'password' => $data[0]['password'],
            'nama' => $data[0]['nama'],
            'kelompok' => $data[0]['kelompok_id'],
            'status' => $data[0]['is_active']
        );

        //$this->myDebug($hasil);
        echo json_encode($hasil);
    }
    
    function delete_user($id){
        
        $pengguna = array(
            'id' => $id,
            'is_active' => '0',
            'modified_date' => date('Y-m-d H:i:s'),
            'modified_by' => $this->session->userdata('username')
        );
        $this->Administrator_model->simpan($pengguna);

    }

    function add_ajax_kelompok() {
        $result = $this->Administrator_model->get_data_kelompok();
        $data = "<option value=''>- Select Kelompok -</option>";
        foreach ($result->result() as $value) {
            $data .= "<option value='" . $value->id . "'>" . $value->nama . "</option>";
        }
        echo $data;
    }

    // </editor-fold>


    function hak_akses() {
        $this->data['content'] = $this->load->view('administrator/hak_akses', $this->data, TRUE);
        $this->load->view('layout/layout_utama', $this->data);
    }
    
    function ajax_data_kelompok(){
        $list_data = $this->Administrator_model->get_data_kelompok();
        $data = array();
        $no = 1;
        foreach ($list_data as $item) {
            $row = array();
            $row[] = $no;
            $row[] = $item['nama'];
            $row[] = $item['keterangan'];
            $row[] = '<button class="btn btn-warning" onclick="kelompok('.$item['id'].')"> Edit </button> &nbsp;&nbsp;'
                    . '<button class="btn btn-danger" onclick="delete('.$item['id'].')"> Hapus </button>';
            $row[] = '<a href="'.  base_url().'administrator/hak_akses_edit/'.$item['id'].'" class="btn btn-success">Hak Akses</a>';
            $data[] = $row;
            $no++;
        }
        $output = array(
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    function get_data_kelompok($kelompok_id){
        $kelompok = $this->Administrator_model->get_kelompok($kelompok_id);
        if(!empty($kelompok)){
            $hasil = array(
                'kelompok_id' => $kelompok['id'],
                'nama' => $kelompok['nama'],
                'keterangan' => $kelompok['keterangan']
            );
        }else{
            $hasil = array(
                'kelompok_id' => '',
                'nama' => '',
                'keterangan' => ''
            );            
        }
        echo json_encode($hasil);
    }
    
    function hak_akses_edit($kelompok_id){
        $data = $this->input->post();
        if(!empty($data)){
            $this->myDebug($data);
        }
        $kelompok = $this->Administrator_model->get_kelompok($kelompok_id);
        $this->data['kelompok'] = $kelompok;
        $list_menu = $this->Administrator_model->get_list_menu($kelompok_id);
        $this->data['list_menu'] = $list_menu;
        $this->data['content'] = $this->load->view('administrator/hak_akses_edit', $this->data, TRUE);
        $this->load->view('layout/layout_utama', $this->data);
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Menu LAPORAN AKTIVITAS ">
    function laporan_aktivitas(){
        $this->data['content'] = $this->load->view('administrator/laporan_aktivitas', $this->data, TRUE);
        $this->load->view('layout/layout_utama', $this->data);
    }

    function ajax_laporan_aktivitas() {
        $awal = '';
        $akhir = '';
        $list_data = $this->Administrator_model->get_laporan_aktivitas($awal, $akhir);
        $data = array();
        $no = 1;
        foreach ($list_data as $item) {
            $row = array();
            $row[] = $no;
            $row[] = $item['created'];
            $row[] = $item['ip_address'];
            $row[] = $item['username'];
            $row[] = $item['aktivitas'];
            $row[] = '<button class="btn btn-warning" onclick="detail(' . $item['id'] . ')"> Data </button>';
            $data[] = $row;
            $no++;
        }
        $output = array(
            "data" => $data,
        );
        echo json_encode($output);
    }

    function get_data_aktivitas($log_aktivitas_id) {
        $aktivitas = $this->Administrator_model->get_aktivitas($log_aktivitas_id);
        if (!empty($aktivitas)) {
            $hasil = array(
                'ip_address' => $aktivitas['ip_address'],
                'username' => $aktivitas['username'],
                'aktivitas' => $aktivitas['aktivitas'],
                'konten' => $aktivitas['konten']
            );
        } else {
            $hasil = array(
                'ip_address' => '',
                'username' => '',
                'aktivitas' => '',
                'konten' => ''
            );
        }
        echo json_encode($hasil);
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Menu LAPORAN ERROR ">
    function laporan_error(){
        $error = $this->db->get('log_error')->row_array();
        $this->data['error'] = $error;
        $this->data['content'] = $this->load->view('administrator/laporan_error', $this->data, TRUE);
        $this->load->view('layout/layout_utama', $this->data);
    }

    function ajax_laporan_error() {
        $awal = '';
        $akhir = '';
        $list_data = $this->Administrator_model->get_laporan_error($awal, $akhir);
        $data = array();
        $no = 1;
        foreach ($list_data as $item) {
            $row = array();
            $row[] = $no;
            $row[] = $item['created'];
            $row[] = $item['ip_address'];
            $row[] = $item['username'];
            $row[] = $item['aktivitas'];
            $row[] = $item['error_message'];
            $row[] = '<button class="btn btn-warning" onclick="detail(' . $item['id'] . ')"> Data </button>';
            $data[] = $row;
            $no++;
        }
        $output = array(
            "data" => $data,
        );
        echo json_encode($output);
    }

    function get_data_error($log_error_id) {
        $aktivitas = $this->Administrator_model->get_error($log_error_id);
        if (!empty($aktivitas)) {
            $hasil = array(
                'ip_address' => $aktivitas['ip_address'],
                'username' => $aktivitas['username'],
                'aktivitas' => $aktivitas['aktivitas'],
                'konten' => $aktivitas['konten'],
                'error_message' => $aktivitas['error_message']
            );
        } else {
            $hasil = array(
                'ip_address' => '',
                'username' => '',
                'aktivitas' => '',
                'konten' => '',
                'error_message' => ''
            );
        }
        echo json_encode($hasil);
    }
    // </editor-fold>
    
}

?>