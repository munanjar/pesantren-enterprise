<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->alert->check_login();
        $this->load->model('Anggota_model');
        $this->load->model('Aktivasi_model');
    }
    
    function index() {
        $data = $this->input->post();
        if (!empty($data)) {
            try {
                $provinsi = $this->db->get_where('wilayah_provinsi', array('provinsi_id' => $data['provinsi']))->row_array();
                $kabupaten = $this->db->get_where('wilayah_kabupaten', array('kabupaten_id' => $data['kabupaten']))->row_array();
                
                $anggota = array(
                    'id'=>$data['id'],
                    //'parent_id' => $data['kyai_id'], 
                    //'userid' => '', //generate
                    //'parent_userid' => $data['kyai_id'], //select parent
                    //'tanggal' => date('Y-m-d H:i:s'),
                    'nama_lengkap' => $data['nama_lengkap'],
                    'tgl_lahir' => $data['tgl_lahir'],
                    'tempat_lahir' => $data['tempat_lahir'],
                    'alamat' => $data['alamat'],
                    'kabupaten_id' => $data['kabupaten'],
                    'kabupaten' => !empty($kabupaten['nama']) ? $kabupaten['nama'] : '',
                    'provinsi_id' => $data['provinsi'],
                    'provinsi' => !empty($provinsi['nama']) ? $provinsi['nama'] : '',
                    'kode_pos' => $data['kode_pos'],
                    'telp' => $data['telp'],
                    'email' => $data['email'],
                    'status_nikah' => $data['status_nikah'],
                    'jenis_kelamin' => $data['jenis_kelamin'],
                    'no_identitas' => $data['no_identitas'],
                    //'status' => 'DAFTAR',
                    //'kelompok' => 'USTADZ', //kelompok ustadz
                    'modified_date' => date('Y-m-d H:i:s'),
                    'modified_by' => $this->session->userdata('username')
                );
                if (!$this->Anggota_model->simpan($anggota)) {
                    throw new Exception('Error ketika update data registrasi ustadz');
                }
                
                $this->simpan_aktivitas('Update Data Registrasi', $data);
                
                $this->session->set_flashdata('message_ok', 'Data Registrasi Telah Diperbaharui');
                redirect('registrasi');
            } catch (Exception $exc) {
                $msg = $exc->getMessage();
                $this->simpan_error('Update Data Registrasi', $data, $exc);
            }
        }
        
        $this->data['content'] = $this->load->view('registrasi/index', $this->data, TRUE);
        $this->load->view('layout/layout_utama', $this->data);
    }
    
    function register_kyai() {
        $data = $this->input->post();
        
        if (!empty($data)) {
            try {
                $provinsi = $this->db->get_where('wilayah_provinsi', array('provinsi_id' => $data['provinsi']))->row_array();
                $kabupaten = $this->db->get_where('wilayah_kabupaten', array('kabupaten_id' => $data['kabupaten']))->row_array();
                $anggota = array(
                    'parent_id' => '',
                    'userid' => '',
                    'parent_userid' => '',
                    'tanggal' => date('Y-m-d H:i:s'),
                    'nama_lengkap' => $data['nama_lengkap'],
                    'tempat_lahir' => $data['tempat_lahir'],
                    'tgl_lahir' => date('Y-m-d', strtotime($data['tgl_lahir'])),
                    'alamat' => $data['alamat'],
                    'kabupaten_id' => $data['kabupaten'],
                    'kabupaten' => !empty($kabupaten['nama']) ? $kabupaten['nama'] : '',
                    'provinsi_id' => $data['provinsi'],
                    'provinsi' => !empty($provinsi['nama']) ? $provinsi['nama'] : '',
                    'kode_pos' => $data['kode_pos'],
                    'telp' => $data['no_telp'],
                    'email' => $data['email'],
                    'status_nikah' => $data['status_nikah'],
                    'jenis_kelamin' => $data['jenis_kelamin'],
                    'no_identitas' => $data['no_identitas'],
                    'npwp' => $data['npwp'],
                    'bank' => $data['bank'],
                    'no_rek' => $data['no_rek'],
                    'nama_rek' => $data['nama_rek'],
                    'status' => 'DAFTAR',
                    'kelompok' => 'KYAI',
                    'modified_date' => date('Y-m-d H:i:s'),
                    'modified_by' => $this->session->userdata('username')
                );
                if (!$this->Anggota_model->simpan($anggota)) {
                    throw new Exception('Error ketika simpan data registrasi kyai');
                }
                $this->simpan_aktivitas('Registrasi Kyai', $data);
                $this->session->set_flashdata('message_ok', 'Data Registrasi Telah Tersimpan');
                redirect('registrasi');
            } catch (Exception $exc) {
                $msg = $exc->getMessage();
                $this->simpan_error('Registrasi Kyai', $data, $exc);
                $this->session->set_flashdata('message_err', $msg);
            }
        }
        $this->data['content'] = $this->load->view('registrasi/register_kyai', $this->data, TRUE);
        $this->load->view('layout/layout_utama', $this->data);
    }

    function register_ustadz() {
        $data = $this->input->post();
        if (!empty($data)) {
            try {
                $provinsi = $this->db->get_where('wilayah_provinsi', array('provinsi_id' => $data['provinsi']))->row_array();
                $kabupaten = $this->db->get_where('wilayah_kabupaten', array('kabupaten_id' => $data['kabupaten']))->row_array();
                $kyai = $this->Anggota_model->get_anggota($data['kyai_id']);
                $anggota = array(
                    'parent_id' => $data['kyai_id'], 
                    'userid' => '',
                    'parent_userid' => $kyai['userid'],
                    'tanggal' => date('Y-m-d H:i:s'),
                    'nama_lengkap' => $data['nama_lengkap'],
                    'tgl_lahir' => $data['tgl_lahir'],
                    'tempat_lahir' => $data['tempat_lahir'],
                    'alamat' => $data['alamat'],
                    'kabupaten_id' => $data['kabupaten'],
                    'kabupaten' => !empty($kabupaten['nama']) ? $kabupaten['nama'] : '',
                    'provinsi_id' => $data['provinsi'],
                    'provinsi' => !empty($provinsi['nama']) ? $provinsi['nama'] : '',
                    'kode_pos' => $data['kode_pos'],
                    'telp' => $data['no_telp'],
                    'email' => $data['email'],
                    'status_nikah' => $data['status_nikah'],
                    'jenis_kelamin' => $data['jenis_kelamin'],
                    'no_identitas' => $data['no_identitas'],
                    'status' => 'DAFTAR',
                    'kelompok' => 'USTADZ', 
                    'modified_date' => date('Y-m-d H:i:s'),
                    'modified_by' => $this->session->userdata('username')
                );
                if (!$this->Anggota_model->simpan($anggota)) {
                    throw new Exception('Error ketika simpan data registrasi ustadz');
                }                
                $this->simpan_aktivitas('Registrasi & Aktivasi Ustadz', $data);
                $this->session->set_flashdata('message_ok', 'Data Registrasi Telah Tersimpan');
                redirect('registrasi');
            } catch (Exception $exc) {
                $msg = $exc->getMessage();
                $this->simpan_error('Registrasi Ustadz', $data, $exc);
                $this->session->set_flashdata('message_err', $msg);
            }
        }
        
        $this->data['content'] = $this->load->view('registrasi/register_ustadz', $this->data, TRUE);
        $this->load->view('layout/layout_utama', $this->data);
    }
    
    function add_ajax_prov(){
        $query = $this->db->get('wilayah_provinsi');
        $data = "<option value=''>- Select Provinsi -</option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='".$value->provinsi_id."'>".$value->nama."</option>";
        }
        echo $data;
    }
    
    function add_ajax_kab($id_prov){
        $query = $this->db->get_where('wilayah_kabupaten',array('provinsi_id'=> $id_prov ));
        $data = "<option value=''>- Select Kabupaten -</option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='".$value->kabupaten_id."'>".$value->nama."</option>";
        }
        echo $data;
    }
    
    function add_ajax_kab_detail(){
        $query = $this->db->get('wilayah_kabupaten');
        $data = "<option value=''>- Select Kabupaten -</option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='".$value->kabupaten_id."'>".$value->nama."</option>";
        }
        echo $data;
    }
    
    function register_jamaah() {
        $data = $this->input->post();
        if (!empty($data)) {
            try {
                $provinsi = $this->db->get_where('wilayah_provinsi', array('provinsi_id' => $data['provinsi']))->row_array();
                $kabupaten = $this->db->get_where('wilayah_kabupaten', array('kabupaten_id' => $data['kabupaten']))->row_array();
                $ustadz = $this->Anggota_model->get_anggota($data['ustadz_id']);
                $anggota = array(
                    'parent_id' => $data['ustadz_id'], 
                    'userid' => '',
                    'parent_userid' => $ustadz['userid'],
                    'tanggal' => date('Y-m-d H:i:s'),
                    'nama_lengkap' => $data['nama_lengkap'],
                    'tgl_lahir' => $data['tgl_lahir'],
                    'tempat_lahir' => $data['tempat_lahir'],
                    'alamat' => $data['alamat'],
                    'kabupaten_id' => $data['kabupaten'],
                    'kabupaten' => !empty($kabupaten['nama']) ? $kabupaten['nama'] : '',
                    'provinsi_id' => $data['provinsi'],
                    'provinsi' => !empty($provinsi['nama']) ? $provinsi['nama'] : '',
                    'kode_pos' => $data['kode_pos'],
                    'telp' => $data['no_telp'],
                    'email' => $data['email'],
                    'status_nikah' => $data['status_nikah'],
                    'jenis_kelamin' => $data['jenis_kelamin'],
                    'no_identitas' => $data['no_identitas'],
                    'status' => 'DAFTAR',
                    'kelompok' => 'JAMAAH', 
                    'modified_date' => date('Y-m-d H:i:s'),
                    'modified_by' => $this->session->userdata('username')
                );
                if (!$this->Anggota_model->simpan($anggota)) {
                    throw new Exception('Error ketika simpan data registrasi Jamaah');
                }                
                $this->simpan_aktivitas('Registrasi & Aktivasi Jamaah', $data);                
                $this->session->set_flashdata('message_ok', 'Data Registrasi Jamaah Telah Tersimpan');
                redirect('registrasi');
            } catch (Exception $exc) {
                $msg = $exc->getMessage();
                $this->simpan_error('Registrasi Jamaah', $data, $exc);
                $this->session->set_flashdata('message_err', $msg);
            }
        }
        $this->data['content'] = $this->load->view('registrasi/register_jamaah', $this->data, TRUE);
        $this->load->view('layout/layout_utama', $this->data);
    }

    function search_kyai(){
        $keyword = $this->uri->segment(3);
        $data = $this->Anggota_model->search_kyai($keyword);
        foreach ($data as $row) {
            $arr['query'] = $keyword;
            $arr['suggestions'][] = array(
                'value' => $row['nama'],
                'anggota_id' => $row['id'],
            );
        }
        echo json_encode($arr);
    }
    
    function search_ustadz(){
        $keyword = $this->uri->segment(3);
        $data = $this->Anggota_model->search_ustadz($keyword);
        foreach ($data as $row) {
            $arr['query'] = $keyword;
            $arr['suggestions'][] = array(
                'value' => $row['nama'],
                'anggota_id' => $row['id'],
            );
        }
        echo json_encode($arr);
    }
    
  
    
    function aktivasi() {
        $data = $this->input->post();
        if (!empty($data)) {
            try {
                $data['nominal_bayar'] = $this->get_number_from_string($data['nominal_bayar']);
                
                if(!$this->Aktivasi_model->aktivasi($data)){
                    throw new Exception('Ada kesalahan ketika simpan data aktivasi anggota');
                }
                
                $this->simpan_aktivitas('Aktivasi Anggota', $data);
                $this->session->set_flashdata('message_ok', 'Data Aktivasi Berhasil Disimpan');
            } catch (Exception $exc) {
                $msg = $exc->getMessage();
                $this->simpan_error('Aktivasi Anggota', $data, $exc);
                $this->session->set_flashdata('message_err', $msg);
            }
        }
        $this->data['content'] = $this->load->view('registrasi/aktivasi', $this->data, TRUE);
        $this->load->view('layout/layout_utama', $this->data);
    }

    function ajax_data_aktivasi() {
        $list_data = $this->Aktivasi_model->get_all_data();
        $data = array();
        $no = 1;
        foreach ($list_data as $item) {
            $row = array();
            $row[] = $no;
            $row[] = $item['nama_lengkap'];
            $row[] = $item['parent_userid'];
            $row[] = $item['telp'];
            $row[] = $item['kode_unik'];
            $row[] = number_format($item['nominal'],0,',','.');
            $row[] = '<button class="btn btn-warning" onclick="aktivasi('.$item['id'].')"> Aktivasi </button>';
            $data[] = $row;
            $no++;
        }
        $output = array(
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    function get_data_anggota($aktivasi_id){
        $aktivasi = $this->Aktivasi_model->get_aktivasi($aktivasi_id);
        $anggota = $this->Anggota_model->get_anggota($aktivasi['anggota_id']);
        if(!empty($anggota)){
            $referal = $this->Anggota_model->get_anggota($anggota['parent_id']);
            $referal_nama = !empty($referal['userid']) ? $referal['userid'].' - '.$referal['nama_lengkap'] : '';
            $hasil = array(
                'aktivasi_id'=>$aktivasi_id,
                'nama_lengkap'=>$anggota['nama_lengkap'],
                'parent_nama'=>$referal_nama,
                'alamat'=>$anggota['alamat'],
                'nominal_bayar'=>number_format($aktivasi['nominal']+$aktivasi['kode_unik'],0,',','.')
            );
        }else{
            $hasil = array(
                'aktivasi_id'=>'',
                'nama_lengkap'=>'',
                'parent_nama'=>'',
                'alamat'=>'',
                'nominal_bayar'=>'',
            );            
        }
        echo json_encode($hasil);
    }

    function lap_registrasi(){
        $this->data['content'] = $this->load->view('registrasi/lap_registrasi', $this->data, TRUE);
        $this->load->view('layout/layout_utama', $this->data);
    }
    
    function ajax_lap_registrasi() {
        $awal = !empty($this->uri->segment(3)) ? $this->uri->segment(3) : '';
        $akhir = !empty($this->uri->segment(4)) ? $this->uri->segment(4) : '';
        
        $list_data = $this->Aktivasi_model->get_lap_registrasi($awal, $akhir);
        $data = array();
        $no = 1;
        foreach ($list_data as $item) {
            $referal = $this->Anggota_model->get_anggota($item['parent_id']);
            $referal_name = !empty($referal) ? $referal['userid'].' - '.$referal['nama_lengkap'] : '';
            $row = array();
            $row[] = $no;
            $row[] = '<strong>Daftar</strong> <br>'.date('Y-m-d', strtotime($item['tanggal_daftar'])).'<br><strong>Aktif</strong> <br>'.$item['tanggal_aktif'];
            $row[] = $item['userid'].' - '.$item['nama_lengkap'];
            $row[] = $referal_name;
            $row[] = '<strong>Kelompok</strong> '.$item['kelompok'].'<br><strong>Status</strong> '.$item['status'];
            $row[] = number_format($item['nominal_bayar'],0,',','.');
            //$row[] = '<button class="btn btn-warning" onclick="detail('.$item['id'].')"> Detail </button>';
            $data[] = $row;
            $no++;
        }
        $output = array(
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    function export_lap_registrasi() {
        $post_data = $this->input->post(NULL, FALSE);
        
        
        $awal = !empty($post_data['awal']) ? $post_data['awal'] : '';
        $akhir = !empty($post_data['akhir']) ? $post_data['akhir'] : '';
        
        $hasil = $this->Aktivasi_model->get_lap_registrasi($awal, $akhir);
        //$data = array();
        $no = 1;
        foreach ($hasil as $item) {
            $no++;
//            $aktivasi = $this->get_data_aktivasi($item['id_booking']);
//            $booking = $this->get_data_booking($item['id_booking']);
//            $product = $this->get_data_product($item['id_product']);
//            $tipe_jamaah = $this->get_data_tipe_jamaah($item['id_tipe_jamaah']);
            
            $referal = $this->Anggota_model->get_anggota($item['parent_id']);
            $referal_nama = !empty($referal['userid']) ? $referal['userid'].' - '.$referal['nama_lengkap'] : '';
            
            $row = array();
            $item['id'] = $item['id'];
            $item['parent_id'] = $referal_nama;//$item['parent_id'];
            $item['userid'] = $item['userid'];
            $item['tgl_daftar'] = date('d/m/Y', strtotime($item['tgl_daftar']));
            $item['nama_lengkap'] = $item['nama_lengkap'];
            $item['tgl_aktif'] = date('d/m/Y', strtotime($item['tgl_daftar']));
            $item['nominal_bayar'] = number_format($item['nominal_bayar']);
            $item['cara_bayar'] = $item['cara_bayar'];
            $item['bank_bayar'] = $item['bank_bayar'];
            $item['status'] = $item['status'];
            $item['kelompok'] = $item['kelompok'];
            
            $data[] = $item;
        }
        $data['list'] = $data;
        
        $this->load->view('registrasi/lap_registrasi_excel', ($data));
    }
    
    function ajax_data_anggota(){
        $list_data = $this->Anggota_model->get_data();
        $data = array();
        $no = 1;
        foreach ($list_data as $item) {
            $row = array();
            $row[] = $no;
            $row[] = $item['nama_lengkap'];
            $row[] = $item['kabupaten'];
            $row[] = ($item['tanggal']!='0000-00-00 00:00:00')? date('d-m-Y', strtotime($item['tanggal'])) : '-';            $row[] = $item['telp'];
            $row[] = $item['email'];
            $row[] = $item['status'];
            $row[] = $item['kelompok'];
            
            $btnDetail = '<button class="btn btn-warning" onclick="detail('.$item['id'].')"> Detail </button>';
            $btnEdit = '<button class="btn btn-info" onclick="edit_data('.$item['id'].')"> Edit </button>';
            $disabled = ($item['status']=="HAPUS") ? 'disabled' : '';
            $btnDelete = '<button class="btn btn-danger" onclick="delete_data('.$item['id'].')" '.$disabled.'> Delete </button>';
            
            $row[] = $btnDetail. ' ' .$btnEdit.' '.$btnDelete;
            $data[] = $row;
            $no++;
        }
        $output = array(
            "data" => $data,
        );
       
        echo json_encode($output);
    }
    
    function detail($id){
        $data = $this->Anggota_model->get_data_edit($id);
        $tanggal = ($data['tanggal'])!='0000-00-00 00:00:00'? date('d-m-Y', strtotime($data['tanggal'])) : '-';
        //$provinsi = $this->wilayah_provinsi($data['provinsi']);
        
        $hasil = array(
                'id'=>$data['id'],
                'nama_lengkap'=>$data['nama_lengkap'],
                'tempat_lahir'=>$data['tempat_lahir'],
                'tgl_lahir'=>$data['tgl_lahir'],
                'jenis_kelamin'=>$data['jenis_kelamin'],
                'tanggal'=>$tanggal,
                'alamat'=>$data['alamat'],
                'kabupaten_id'=>$data['kabupaten_id'],
                'provinsi_id'=>$data['provinsi_id'],                
                'kode_pos'=>$data['kode_pos'],
                'telp'=>$data['telp'],
                'email'=>$data['email'],
                'no_identitas'=>$data['no_identitas'],
                'status_nikah'=>$data['status_nikah'],
                'kelompok'=>$data['kelompok'],
                
            );
        
        //$this->myDebug($hasil);
        echo json_encode($hasil);
    }
    
    function delete_data($id_anggota){
        $send = $this->Anggota_model->delete($id_anggota);
    }
    
    function wilayah_provinsi($id){
        return $this->Anggota_model->get_data_prov($id);
    }
    
    function add_ajax_bank(){
        $query = $this->db->get_where('referensi', array('kelompok'=>'BANK', 'ORDER BY nomor ASC'));
        $data = "<option value=''>- Select Bank -</option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='".$value->item."'>".$value->item."</option>";
        }
        echo $data;
    }
}

?>