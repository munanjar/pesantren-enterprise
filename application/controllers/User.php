<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('User_model', 'user', TRUE);
    }

    function index() {
        $this->data['form_action'] = site_url('user/do_login');
        $this->data['content'] = $this->load->view('user/login', $this->data, TRUE);
        $this->load->view('layout/layout_login', $this->data);
    }
    
    function do_login(){
        $data = $this->input->post(NULL, TRUE);
        if (!empty($data)) {
            // 1. validasi user
            $user = $data['u_name'];
            $pass = $data['u_pass'];
            $cek = $this->user->cek_user($user, $pass);
            if (!empty($cek)){
                $anggota = $this->user->get_data_anggota($cek['anggota_id']);
                $arr_session = array(
                    'username' => $cek['username'],
                    'email' => $anggota['email'],
                    'login' => TRUE
                );
                $this->session->set_userdata($arr_session);
                redirect('home/dashboard');
            }else{
                $this->session->set_flashdata('message_war', 'Username & Password Tidak Sesuai');

                redirect('user');
            }
            
            // 2. get hak akses user
            
            // 3. redirect
            
        }
    }
    
    function logout(){
        $this->session->sess_destroy();
        redirect('user');
    }
    

    function test() {
        $this->data['content'] = $this->load->view('user/test', $this->data, TRUE);
        $this->load->view('layout/layout_utama', $this->data);
    }
    
    function profile(){
        $this->data['content'] = $this->load->view('user/profile', $this->data, TRUE);
        $this->load->view('layout/layout_utama', $this->data);
    }
    
    function setting(){
        $this->data['content'] = $this->load->view('user/setting', $this->data, TRUE);
        $this->load->view('layout/layout_utama', $this->data);
    }
    
    

}
?>