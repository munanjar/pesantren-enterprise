<?php

/*
  Fungsi : Semua fungsi-fungsi global ditaruh disini
 */

Class Alert {

    public function __construct() {
        // Constructor's functionality here, if you have any.
        $this->CI = & get_instance();
    }

    function Alert() {
        /* Load Library CI */

        $this->CI->load->library(array('session', 'form_validation'));
        //$this->CI->load->helper('url');

        $this->load->helper('form');
        $this->load->helper('url');
    }

    /* Display system message ok & error */

    function disp_sys_msg() {
        echo $this->CI->session->flashdata('message_ok') == '' ? '' : '<div class="alert alert-success alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
</button>&nbsp;' . $this->CI->session->flashdata('message_ok') . '</div>';
        echo $this->CI->session->flashdata('message_err') == '' ? '' : '<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
</button>&nbsp;' . $this->CI->session->flashdata('message_err') . '</div>';
        echo $this->CI->session->flashdata('message_war') == '' ? '' : '<div class="alert alert-warning alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span>
</button>&nbsp;' . $this->CI->session->flashdata('message_war') . '</div>';
    }
    
    /* Fungsi utk check session user */
    function check_login()
    {	
        if($this->CI->session->userdata('login')==FALSE)
        {	
                $this->CI->session->set_flashdata('message_err', 'Please login to enter system');
                redirect('User');
                //redirect('Home/');
        }
    }

}
