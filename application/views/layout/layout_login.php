<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Vendor styles -->
        <link rel="stylesheet" href="<?php echo base_url().'assets/Bootstrap4/' ?>vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="<?php echo base_url().'assets/Bootstrap4/' ?>vendors/bower_components/animate.css/animate.min.css">
        <!-- App styles -->
        <link rel="stylesheet" href="<?php echo base_url().'assets/Bootstrap4/' ?>css/app.min.css">
    </head>

    <body data-ma-theme="green">
        <?php echo $content ?>
        
        <!-- Javascript -->
        <!-- Vendors -->
        <script src="<?php echo base_url().'assets/Bootstrap4/' ?>vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo base_url().'assets/Bootstrap4/' ?>vendors/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="<?php echo base_url().'assets/Bootstrap4/' ?>vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url().'assets/Bootstrap4/' ?>vendors/bower_components/Waves/dist/waves.min.js"></script>

        <!-- App functions and actions -->
        <script src="<?php echo base_url().'assets/Bootstrap4/' ?>js/app.min.js"></script>
    </body>
</html>