<header class="content__title">
    <h1>Jurnal Harian</h1>
    <div class="actions">
        <a href="" class="actions__item zmdi zmdi-trending-up"></a>
        <a href="" class="actions__item zmdi zmdi-check-all"></a>
        <div class="dropdown actions__item">
            <i data-toggle="dropdown" class="zmdi zmdi-more-vert"></i>
            <div class="dropdown-menu dropdown-menu-right">
                <a href="" class="dropdown-item">Refresh</a>
                <a href="" class="dropdown-item">Manage Widgets</a>
                <a href="" class="dropdown-item">Settings</a>
            </div>
        </div>
    </div>
</header>

<div class="card">
    <div class="card-header">
        <h2 class="card-title">Jurnal Harian</h2>
        <small class="card-subtitle">Data Transaksi Harian Pesantren</small>
    </div>

    <div class="card-block">
        <form method="POST" id="form_aktivasi" name="form_aktivasi">
        </form>
        <div class="table-responsive">
            <table id="example1" class="table table-bordered table-condensed table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Akun</th>
                        <th>Group Transaksi</th>
                        <th>Keterangan</th>
                        <th>Debet</th>
                        <th>Kredit</th>
                        <th>Saldo</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Vendors -->
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/tether/dist/js/tether.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/Waves/dist/waves.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>

<!-- Vendors: Data tables -->
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jszip/dist/jszip.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>


<script type="text/javascript">
    var table;
    $(document).ready(function () {
        table = $('#example1').DataTable({
            "ajax": {
                "url": "<?php echo site_url('pembayaran/ajax_jurnal_harian') ?>",
            },
            "columnDefs": [
                {
                    "targets": [0], 
                    "orderable": false, 
                },
            ],
        });
    });
    
</script>