<header class="content__title">
    <h1>Angsuran Anggota</h1>

    <div class="actions">
        <a href="" class="actions__item zmdi zmdi-trending-up"></a>
        <a href="" class="actions__item zmdi zmdi-check-all"></a>

        <div class="dropdown actions__item">
            <i data-toggle="dropdown" class="zmdi zmdi-more-vert"></i>
            <div class="dropdown-menu dropdown-menu-right">
                <a href="" class="dropdown-item">Refresh</a>
                <a href="" class="dropdown-item">Manage Widgets</a>
                <a href="" class="dropdown-item">Settings</a>
            </div>
        </div>
    </div>
</header>

<div class="card">
    <div class="card-header">
        <h2 class="card-title">Data Angsuran Anggota</h2>
        <small class="card-subtitle"></small>
    </div>

    <div class="card-block">
        <div class="modal fade" id="modal-default" tabindex="-1">
            <div class="modal-dialog">
                <form method="POST" id="form_angsuran" name="form_angsuran">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title pull-left">Data Anggota</h5>
                        </div>
                        <div class="modal-body">
                            <div class="form-group" style="margin: 0px;">
                                <input type="hidden" name="anggota_id" id="anggota_id" class="form-control">
                                <label>Nama lengkap</label>
                                <input type="text" name="nama_lengkap" id="nama_lengkap" class="form-control" readonly="">
                                <i class="form-group__bar"></i>
                            </div>
                            <div class="form-group">
                                <label>Alamat</label>
                                <input type="text" name="alamat" id="alamat" class="form-control" required="">
                                <i class="form-group__bar"></i>
                            </div>
                            <div class="form-group form-group--float">
                                <select required class="form-control" name="cara_bayar" id="cara_bayar">
                                    <option value="CASH">CASH</option>
                                    <option value="TRANSFER">TRANSFER</option>
                                </select>
                                <label>Cara Bayar</label>
                                <i class="form-group__bar"></i>
                            </div>
                            <div class="form-group">
                                <label>Nominal Pembayaran</label>
                                <input type="text" name="nominal_bayar" id="nominal_bayar" class="form-control" required="">
                                <i class="form-group__bar"></i>
                            </div>
                            <div id="hidden_div" style="display: none;">
                                <div class="form-group form-group--float">
                                    <select required class="form-control" name="bank_transfer" id="bank_transfer">
                                        <option value="">-- Pilih Bank --</option>
                                        <option value="BCA">BCA</option>
                                        <option value="BNI">BNI</option>
                                        <option value="BRI">BRI</option>
                                        <option value="MANDIRI">MANDIRI</option>
                                    </select>
                                    <label>Bank Transfer</label>
                                    <i class="form-group__bar"></i>
                                </div>
                                <div class="form-group">
                                    <label>Bukti Pembayaran</label>
                                    <input type="file" name="bukti_pembayaran" id="bukti_pembayaran" class="form-control" required="">
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="form-group form-group--float">
                                <input type="text" name="keterangan" id="keterangan" class="form-control" required="">
                                <label>Keterangan</label>
                                <i class="form-group__bar"></i>
                            </div>
                            <div class="form-group">
                                <label>Tanggal Bayar</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                    <div class="form-group">
                                        <input type="text" class="form-control date-picker" name="tgl_bayar" id="tgl_bayar" placeholder="Pick a date" required="">
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link" onclick="simpan_angsuran()">Simpan</button>
                            <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Anggota</th>
                        <th>Kelompok</th>
                        <th>Total</th>
                        <th>Angsuran</th>
                        <th>Jml Angsuran</th>
                        <th>Sisa</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Vendors -->
<script src="<?php echo base_url().'assets/Bootstrap4/' ?>vendors/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url().'assets/Bootstrap4/' ?>vendors/bower_components/tether/dist/js/tether.min.js"></script>
<script src="<?php echo base_url().'assets/Bootstrap4/' ?>vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url().'assets/Bootstrap4/' ?>vendors/bower_components/Waves/dist/waves.min.js"></script>
<script src="<?php echo base_url().'assets/Bootstrap4/' ?>vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="<?php echo base_url().'assets/Bootstrap4/' ?>vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>

<!-- Vendors: Data tables -->
<script src="<?php echo base_url().'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url().'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url().'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url().'assets/Bootstrap4/' ?>vendors/bower_components/jszip/dist/jszip.min.js"></script>
<script src="<?php echo base_url().'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>



<script type="text/javascript">
    var table;
    $(document).ready(function() {
        table = $('#example1').DataTable({ 
            "ajax": {
                "url": "<?php echo site_url('pembayaran/ajax_data_angsuran') ?>",
            },
            "columnDefs": [
                { 
                    "targets": [ 0 ], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],
        } );
    });
    function angsuran(anggota_id){
        var link_rm = "<?php echo site_url('pembayaran/get_data_anggota') ?>";
                link_rm = link_rm + "/" + anggota_id;
        $.get(link_rm, function(data){
            $('#anggota_id').val(data.anggota_id);
            $('#nama_lengkap').val(data.nama);
            $('#alamat').val(data.alamat);
            $('#nominal_bayar').val(data.nominal_bayar);
        },"json");
        $('#modal-default').modal('show');
    };
    document.getElementById('cara_bayar').addEventListener('change', function () {
        var style = this.value == 'TRANSFER' ? 'block' : 'none';
        document.getElementById('hidden_div').style.display = style;
        cara_bayar = $(this).val();
        console.log(cara_bayar);
        if (cara_bayar == 'TRANSFER') {
            $("#select_bank").prop('required', true);
            $("#pic1").prop('required', true);
        } else {
            $("#select_bank").prop('required', false);
            $("#pic1").prop('required', false);
        }
    });
    function simpan_angsuran(){
        $('#form_angsuran').submit();
    };
</script>