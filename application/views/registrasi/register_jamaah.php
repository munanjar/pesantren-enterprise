<div class="content__inner">
    <header class="content__title">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>/registrasi">Registrasi</a></li>
            <li class="breadcrumb-item active">Registrasi Jamaah</li>
        </ol>
    </header>
    <form method="POST">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Registrasi Jamaah</h2>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group form-group--float" style="margin-top:0px;">
                            <input type="text" name="nama_lengkap" id="nama_lengkap" class="form-control" required="">
                            <label>Nama lengkap</label>
                            <i class="form-group__bar"></i>
                        </div>                        
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-group--float" style="margin-top:0px;">
                            <input type="search" name="autocomplete" id="autocomplete1" class="form-control autocomplete" tabindex="1" required>
                            <input type="hidden" name="ustadz_id" id="ustadz_id" class="form-control" required="">
                            <label>Ustadz</label>
                            <i class="form-group__bar"></i>
                        </div>                        
                    </div>

                    <div class="col-md-4">
                        <div class="form-group form-group--float" style="margin-top:0px;">
                            <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control">
                            <label>Tempat Lahir</label>
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-md-2">
                    </div>
                    <div class="col-sm-3">
                        
                        <div class="input-group">
                            <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                            <div class="form-group">
                                <input type="text" name="tgl_lahir" id="tgl_lahir" class="form-control date-picker" placeholder="Tgl lahir">
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>

                    <!--
                    <div class="col-md-3">
                        <div class="form-group form-group--float" style="margin-top:0px;">
                            <input type="text" name="tgl_lahir" id="tgl_lahir" class="form-control">
                            <label>Tgl Lahir</label>
                            <i class="form-group__bar"></i>
                        </div>                    
                    </div>
                    -->
                    <div class="col-md-3">
                    </div>

                    <div class="col-md-4">
                        <div class="form-group form-group--float" style="margin-top:0px;">
                            <div class="select">
                                <select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
                                    <option value="">Jenis Kelamin</option>
                                    <option value="Laki-laki">Laki-laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-group--float" style="margin-top:0px;">
                            <div class="select">
                                <select class="form-control" id="status_nikah" name="status_nikah">
                                    <option value="">Status Nikah</option>
                                    <option value="Belum Menikah">Belum Menikah</option>
                                    <option value="Sudah Menikah">Sudah Menikah</option>
                                    <option value="Duda/Janda">Duda/Janda</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                    </div>

                    <div class="col-md-6">
                        <div class="form-group form-group--float" style="margin-top:0px;">
                            <input type="text" name="alamat" id="alamat" class="form-control" required="">
                            <label>Alamat</label>
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-group--float" style="margin-top:0px;">
                            <input type="text" name="kode_pos" id="kode_pos" class="form-control">
                            <label>Kode Pos</label>
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-md-3">
                    </div>

                    <div class="col-md-4">
                        <div class="form-group form-group--float" style="margin-top:0px;">
                            <div class="select">
                                <select class="form-control" id="provinsi" name="provinsi">
                                    <option>Provinsi</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-4">
                        <div class="form-group form-group--float" style="margin-top:0px;">
                            <select class="form-control" id="kabupaten" name="kabupaten">
                                <option>Kabupaten/Kota</option>

                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                    </div>

                    <div class="col-md-4">
                        <div class="form-group form-group--float" style="margin-top:0px;">
                            <input type="text" name="no_telp" id="no_telp" class="form-control" required="">
                            <label>No Handphone</label>
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-group--float" style="margin-top:0px;">
                            <input type="email" name="email" id="email" class="form-control">
                            <label>Email</label>
                            <i class="form-group__bar"></i>
                        </div>                    
                    </div>
                    <div class="col-md-2">
                    </div>

                    <div class="col-md-4">
                        <div class="form-group form-group--float" style="margin-top:0px;">
                            <input type="text" name="no_identitas" id="no_identitas" class="form-control">
                            <label>No Identitas</label>
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-md-8">

                    </div>


                </div>
                <button type="submit" class="btn btn-lg btn-primary">Simpan</button>                
            </div>
        </div>
    </form>
</div>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jquery/dist/jquery.min.js"></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/jquery.autocomplete.js'></script>
<link href='<?php echo base_url(); ?>assets/jquery.autocomplete.css' rel='stylesheet' />

<script type='text/javascript'>
    var site = "<?php echo base_url(); ?>";
    $(function () {
        $('.autocomplete').autocomplete({
            serviceUrl: site + 'registrasi/search_ustadz',
            onSelect: function (suggestion) {
                $('#ustadz_id').val('' + suggestion.anggota_id);
            }
        });

        $('#provinsi').change(function () {
            var url = "<?php echo base_url('registrasi/add_ajax_kab') ?>/" + $(this).val();
            $('#kabupaten').load(url);
            //return false;
        });
    });

    $(document).ready(function () {
        var url = "<?php echo base_url('registrasi/add_ajax_prov') ?>";
        $("#provinsi").load(url);
    });

</script>
