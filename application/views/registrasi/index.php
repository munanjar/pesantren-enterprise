<header class="content__title">
    <h1>Data Anggota</h1>

    <div class="actions">
        <a href="" class="actions__item zmdi zmdi-trending-up"></a>
        <a href="" class="actions__item zmdi zmdi-check-all"></a>

        <div class="dropdown actions__item">
            <i data-toggle="dropdown" class="zmdi zmdi-more-vert"></i>
            <div class="dropdown-menu dropdown-menu-right">
                <a href="" class="dropdown-item">Refresh</a>
                <a href="" class="dropdown-item">Manage Widgets</a>
                <a href="" class="dropdown-item">Settings</a>
            </div>
        </div>
    </div>
</header>

<div class="card">
    <div class="card-header">
        <h2 class="card-title">Data Anggota </h2>
        <small class="card-subtitle">Daftar anggota Pesantren Enterprise</small>
    </div>

    <div class="card-block">
        <div class="modal fade" id="modal-default" data-backdrop="false" tabindex="-1">
            <div class="modal-dialog modal-lg">
                <form method="POST" id="form_aktivasi" name="form_detail">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title pull-left">Data Anggota</h5>
                        </div>
                        <div class="modal-body">
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label style="color: red"><b>Nama lengkap</b></label>
                                            <input type="hidden" name="id" id="id">
                                            <input type="text" name="nama_lengkap" id="nama_lengkap" class="form-control" required="">

                                            <i class="form-group__bar"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-6"></div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label style="color: red"><b>Tempat Lahir</b></label>
                                            <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control" required="">
                                            <i class="form-group__bar"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label style="color: red"><b>Tgl Lahir</b></label>
                                            <div class="form-group">
                                                <input type="text" name="tgl_lahir" id="tgl_lahir" class="form-control date-picker" placeholder="Tgl lahir">
                                                <i class="form-group__bar"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label style="color: red"><b>Jenis Kelamin</b></label>
                                            <div class="form-group">
                                                <select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
                                                    <option value="">Jenis Kelamin</option>
                                                    <option value="LAKI-LAKI">Laki-laki</option>
                                                    <option value="PEREMPUAN">Perempuan</option>
                                                </select>
                                                <!--<input type="text" name="jenis_kelamin" id="jenis_kelamin" class="form-control" placeholder="Jenis Kelamin">-->
                                                <i class="form-group__bar"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label style="color: red"><b>Alamat</b></label>
                                            <input type="text" name="alamat" id="alamat" class="form-control" required="">
                                            <i class="form-group__bar"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label style="color: red"><b>Provinsi</b></label>
                                            <div class="form-group">
                                                <select class="form-control" id="provinsi" name="provinsi">
                                                    <option>Provinsi</option>
                                                </select>
                                                <!--<input type="text" name="provinsi" id="provinsi" class="form-control" placeholder="Tgl lahir">-->
                                                <i class="form-group__bar"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label style="color: red"><b>Kota</b></label>
                                            <div class="form-group">
                                                <!--<input type="text" name="kota" id="kota" class="form-control" placeholder="Tgl lahir">-->
                                                <select class="form-control" id="kota" name="kota">
                                                    <option>Kota</option>
                                                </select>
                                                <i class="form-group__bar"></i>
                                            </div>
                                        </div>
                                    </div>
                                    

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label style="color: red"><b>Kode Pos</b></label>
                                            <div class="form-group">
                                                <input type="text" name="kode_pos" id="kode_pos" class="form-control" placeholder="Tgl lahir">
                                                <i class="form-group__bar"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label style="color: red"><b>Telp</b></label>
                                            <div class="form-group">
                                                <input type="text" name="telp" id="telp" class="form-control" placeholder="No Telp">
                                                <i class="form-group__bar"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label style="color: red"><b>Email</b></label>
                                            <div class="form-group">
                                                <input type="text" name="email" id="email" class="form-control" placeholder="Tgl lahir">
                                                <i class="form-group__bar"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label style="color: red"><b>No. Identitas</b></label>
                                            <div class="form-group">
                                                <input type="text" name="no_identitas" id="no_identitas" class="form-control" placeholder="Status Kawin">
                                                <i class="form-group__bar"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label style="color: red"><b>Status Kawin</b></label>
                                            <div class="form-group">
                                                <select class="form-control" id="status_nikah" name="status_nikah">
                                                    <option value="">Status Nikah</option>
                                                    <option value="BELUM MENIKAH">Belum Menikah</option>
                                                    <option value="SUDAH MENIKAH">Sudah Menikah</option>
                                                    <option value="DUDA/JANDA">Duda/Janda</option>
                                                </select>
                                                                <!--<input type="text" name="status_nikah" id="status_nikah" class="form-control" placeholder="Status Kawin">-->
                                                <i class="form-group__bar"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label style="color: red"><b>Tanggal Daftar</b></label>
                                            <input type="text" name="tgl_daftar" id="tgl_daftar" class="form-control" readonly="readonly">

                                            <i class="form-group__bar"></i>
                                        </div>
                                    </div>  
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label style="color: red"><b>Kelompok</b></label>
                                            <input type="text" name="kelompok" id="kelompok" class="form-control" readonly="readonly">

                                            <i class="form-group__bar"></i>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button id="btnsave" type="submit" class="btn btn-lg btn-primary" onclick="update_data()">Simpan</button>
                            <button type="button" class="btn btn-lg btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table id="example1" class="table table-bordered table-condensed table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Kabupaten</th>
                        <th>Tgl. Daftar</th>
                        <th>Telp</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Kelompok</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Vendors -->
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/tether/dist/js/tether.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/Waves/dist/waves.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>



<script type="text/javascript">
    var table;
    $(document).ready(function () {
        table = $('#example1').DataTable({
            "ajax": {
                "url": "<?php echo site_url('registrasi/ajax_data_anggota') ?>",
            },
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],
        });
    });
    
    function detail(id_anggota) {
        var link_rm = "<?php echo site_url('registrasi/detail') ?>";
        link_rm = link_rm + "/" + id_anggota;
        $.get(link_rm, function (data) {
            $('#id').val(data.id);
            $('#nama_lengkap').val(data.nama_lengkap);
            $('#tempat_lahir').val(data.tempat_lahir);
            $('#tgl_lahir').val(data.tgl_lahir);
            $('#alamat').val(data.alamat);
            $('#tgl_daftar').val(data.tanggal);
            $('#kota').val(data.kabupaten_id);
            $('#provinsi').val(data.provinsi_id);
            $('#kode_pos').val(data.kode_pos);
            $('#telp').val(data.telp);
            $('#email').val(data.email);
            $('#jenis_kelamin').val(data.jenis_kelamin);
            $('#no_identitas').val(data.no_identitas);
            $('#status_nikah').val(data.status_nikah);
            $('#kelompok').val(data.kelompok);
            $('#btnsave').prop('disabled', true); 
        }, "json");

        $('#modal-default').modal('show');
        
    };
    
    function edit_data(id_anggota) {
        var link_rm = "<?php echo site_url('registrasi/detail') ?>";
        link_rm = link_rm + "/" + id_anggota;
        $.get(link_rm, function (data) {
            $('#id').val(data.id);
            $('#nama_lengkap').val(data.nama_lengkap);
            $('#tempat_lahir').val(data.tempat_lahir);
            $('#tgl_lahir').val(data.tgl_lahir);
            $('#alamat').val(data.alamat);
            $('#tgl_daftar').val(data.tanggal);
            $('#kota').val(data.kabupaten_id);
            $('#provinsi').val(data.provinsi_id);
            $('#kode_pos').val(data.kode_pos);
            $('#telp').val(data.telp);
            $('#email').val(data.email);
            $('#jenis_kelamin').val(data.jenis_kelamin);
            $('#no_identitas').val(data.no_identitas);
            $('#status_nikah').val(data.status_nikah);
            $('#kelompok').val(data.kelompok);
            
            $('#btnsave').prop('disabled', false); 

        }, "json");

        $('#modal-default').modal('show');
        
    };

    function delete_data(id)
    {
        if (confirm('Apakah Anda Akan Menghapus Data Ini (' + id + ') '))
        {
            $.ajax({
                url: "<?php echo site_url('registrasi/delete_data') ?>/" + id,
                type: "POST",
                dataType: "JSON",
                success: function (data)
                {
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    table.ajax.reload();
                }
            });
        }
    }

    document.getElementById('select_pay_method').addEventListener('change', function () {
        var style = this.value == 'TRANSFER' ? 'block' : 'none';
        document.getElementById('hidden_div').style.display = style;
        // document.getElementById('hidden_div2').style.display = style;
        // $("#datepicker").prop('required',true);
        select_pay_method = $(this).val();
        console.log(select_pay_method);
        if (select_pay_method == 'TRANSFER') {
            $("#select_bank").prop('required', true);
            $("#pic1").prop('required', true);
        } else {
            $("#select_bank").prop('required', false);
            $("#pic1").prop('required', false);
        }
    });
    
    function simpan_aktivasi() {
        $('#form_aktivasi').submit();
    };
</script>

<script type='text/javascript' src='<?php echo base_url(); ?>assets/jquery.autocomplete.js'></script>
<link href='<?php echo base_url(); ?>assets/jquery.autocomplete.css' rel='stylesheet' />

<script type='text/javascript'>
    var site = "<?php echo base_url(); ?>";
    $(function () {
        $('.autocomplete').autocomplete({
            serviceUrl: site + 'registrasi/search_ustadz',
            onSelect: function (suggestion) {
                $('#ustadz_id').val('' + suggestion.anggota_id);
            }
        });

        $('#provinsi').change(function () {
            var url = "<?php echo base_url('registrasi/add_ajax_kab') ?>/" + $(this).val();
            $('#kabupaten').load(url);
            //return false;
        });
    });

    $(document).ready(function () {
        var url = "<?php echo base_url('registrasi/add_ajax_prov') ?>";
        $("#provinsi").load(url);
        var url = "<?php echo base_url('registrasi/add_ajax_kab_detail') ?>";
        $("#kota").load(url);
    });
    
    

</script>