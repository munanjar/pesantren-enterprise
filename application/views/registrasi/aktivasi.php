<header class="content__title">
    <h1>Aktivasi Anggota</h1>
    <div class="actions">
        <a href="" class="actions__item zmdi zmdi-trending-up"></a>
        <a href="" class="actions__item zmdi zmdi-check-all"></a>
        <div class="dropdown actions__item">
            <i data-toggle="dropdown" class="zmdi zmdi-more-vert"></i>
            <div class="dropdown-menu dropdown-menu-right">
                <a href="" class="dropdown-item">Refresh</a>
                <a href="" class="dropdown-item">Manage Widgets</a>
                <a href="" class="dropdown-item">Settings</a>
            </div>
        </div>
    </div>
</header>

<div class="card">
    <div class="card-header">
        <h2 class="card-title">Aktivasi Anggota</h2>
        <small class="card-subtitle">Daftar calon anggota yang sudah melakukan registrasi, namun belum diaktivasi</small>
    </div>

    <div class="card-block">
        <div class="modal fade" id="modal-default" tabindex="-1">
            <div class="modal-dialog">
                <form method="POST" id="form_aktivasi" name="form_aktivasi">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title pull-left">Aktivasi Anggota</h5>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Nama Anggota</label>
                                <input type="hidden" name="aktivasi_id" id="aktivasi_id" class="form-control" readonly="">
                                <input type="text" name="nama_lengkap" id="nama_lengkap" class="form-control" readonly="">
                                <i class="form-group__bar"></i>
                            </div>
                            
                            <div class="form-group">
                                <label>Referal</label>
                                <input type="text" name="parent_nama" id="parent_nama" class="form-control" readonly="">
                                <i class="form-group__bar"></i>
                            </div>
                            <div class="form-group">
                                <label>Alamat</label>
                                <input type="text" name="alamat" id="alamat" class="form-control" readonly="">
                                <i class="form-group__bar"></i>
                            </div>
                            <div class="form-group form-group--float">
                                <select class="form-control" name="cara_bayar" id="cara_bayar" required="">
                                    <option value="CASH">CASH</option>
                                    <option value="TRANSFER">TRANSFER</option>
                                </select>
                                <label>Cara Bayar</label>
                                <i class="form-group__bar"></i>
                            </div>
                            <div class="form-group">
                                <label>Nominal Pembayaran</label>
                                <input type="text" name="nominal_bayar" id="nominal_bayar" class="form-control" required="">
                                <i class="form-group__bar"></i>
                            </div>
                            <div id="hidden_div" style="display: none;">
                                <div class="form-group form-group--float">
                                    <select required class="form-control" name="bank_transfer" id="bank_transfer">
                                        <option value="">-- Pilih Bank --</option>
                                        <option value="BCA">BCA</option>
                                        <option value="BNI">BNI</option>
                                        <option value="BRI">BRI</option>
                                        <option value="MANDIRI">MANDIRI</option>
                                    </select>
                                    <label>Bank Transfer</label>
                                    <i class="form-group__bar"></i>
                                </div>
                                <div class="form-group">
                                    <label>Bukti Pembayaran</label>
                                    <input type="file" name="bukti_pembayaran" id="bukti_pembayaran" class="form-control" required="">
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="form-group form-group--float">
                                <input type="text" name="keterangan" id="keterangan" class="form-control" required="">
                                <label>Keterangan</label>
                                <i class="form-group__bar"></i>
                            </div>
                            <div class="form-group">
                                <label>Tanggal Bayar</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                    <div class="form-group">
                                        <input type="text" class="form-control date-picker" name="tgl_bayar" id="tgl_bayar" placeholder="Pick a date" required="">
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link" onclick="simpan_aktivasi()">Simpan</button>
                            <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table id="example1" class="table table-bordered table-condensed table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Referal</th>
                        <th>Telp</th>
                        <th>Kode Unik</th>
                        <th>Nominal</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Vendors -->
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/tether/dist/js/tether.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/Waves/dist/waves.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>

<!-- Vendors: Data tables -->
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jszip/dist/jszip.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>


<script type="text/javascript">
    var table;
    $(document).ready(function () {
        table = $('#example1').DataTable({
            "ajax": {
                "url": "<?php echo site_url('registrasi/ajax_data_aktivasi') ?>",
            },
            "columnDefs": [
                {
                    "targets": [0], 
                    "orderable": false, 
                },
            ],
        });
    });
    function aktivasi(id_anggota) {
        var link_rm = "<?php echo site_url('registrasi/get_data_anggota') ?>";
        link_rm = link_rm + "/" + id_anggota;
        $.get(link_rm, function (data) {
            $('#aktivasi_id').val(data.aktivasi_id);
            $('#nama_lengkap').val(data.nama_lengkap);
            $('#parent_nama').val(data.parent_nama);
            $('#alamat').val(data.alamat);
            $('#nominal_bayar').val(data.nominal_bayar);
        }, "json");
        $('#modal-default').modal('show');
    };
    document.getElementById('cara_bayar').addEventListener('change', function () {
        var style = this.value == 'TRANSFER' ? 'block' : 'none';
        document.getElementById('hidden_div').style.display = style;
        cara_bayar = $(this).val();
        console.log(cara_bayar);
        if (cara_bayar == 'TRANSFER') {
            $("#bank_transfer").prop('required', true);
            $("#bukti_pembayaran").prop('required', true);
        } else {
            $("#bank_transfer").prop('required', false);
            $("#bukti_pembayaran").prop('required', false);
        }
    });
    function simpan_aktivasi() {
        $('#form_aktivasi').submit();
    };
</script>