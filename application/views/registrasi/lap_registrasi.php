<header class="content__title">
    <h1>Laporan Registrasi</h1>
    <div class="actions">
        <a href="" class="actions__item zmdi zmdi-trending-up"></a>
        <a href="" class="actions__item zmdi zmdi-check-all"></a>
        <div class="dropdown actions__item">
            <i data-toggle="dropdown" class="zmdi zmdi-more-vert"></i>
            <div class="dropdown-menu dropdown-menu-right">
                <a href="" class="dropdown-item">Refresh</a>
                <a href="" class="dropdown-item">Manage Widgets</a>
                <a href="" class="dropdown-item">Settings</a>
            </div>
        </div>
    </div>
</header>

<div class="card">
    <div class="card-header">
        <h2 class="card-title">Laporan Registrasi Anggota</h2>
        <small class="card-subtitle">Data anggota yang melakukan registrasi</small>
    </div>

    <div class="card-block">
        
        <form method="POST" name="form1" action="<?php echo base_url();?>registrasi/export_lap_registrasi">

            <div class="row">
                <div class="col-md-1">
                    <div class="form-group" style="margin-top:0px;">

                        <label><strong>Filter : </strong></label>   
                    </div>                        
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                        <div class="form-group">
                            <input type="text" name="awal" id="awal" class="form-control date-picker" placeholder="yyyy-mm-dd">

                        </div>
                    </div>                        
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                        <div class="form-group">
                            <input type="text" name="akhir" id="akhir" class="form-control date-picker" placeholder="yyyy-mm-dd">

                        </div>
                    </div>                        
                </div>
                <div class="col-md-1">
                    <div class="form-group" style="margin-top:0px;">
                        <button id="btn-filter" name="btn-filter" type="button" class="btn btn-lg btn-primary">Filter</button>

                    </div>                        
                </div>
                <div class="col-md-2">
                    <div class="form-group" style="margin-top:0px;">

                        <button id="btn-export" name="btn-export" type="submit" class="btn btn-lg btn-success">Export to Excel</button>
                    </div>                        
                </div>
            </div>
            <div class="table-responsive">
                <table id="example1" class="table table-bordered table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th width="15%">Tanggal</th>
                            <th>Nama</th>
                            <th>Sponsor</th>
                            <th>Status</th>
                            <th>Nominal</th>
                            <!--<th>Aksi</th>-->
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </form>
    </div>
</div>

<!-- Vendors -->
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/tether/dist/js/tether.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/Waves/dist/waves.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>

<!-- Vendors: Data tables -->
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jszip/dist/jszip.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>


<script type="text/javascript">
    var table;
    $(document).ready(function () {
        table = $('#example1').DataTable({
            "ajax": {
                "url": "<?php echo site_url('registrasi/ajax_lap_registrasi') ?>",
            },
            "columnDefs": [
                {
                    "targets": [0],
                    "orderable": false,
                },
            ],
        });
    
        $('#btn-filter').click(function () { //button filter event click
            var awal = document.getElementById("awal").value;
            var akhir = document.getElementById("akhir").value;
            table.ajax.url("<?php echo site_url('registrasi/ajax_lap_registrasi'); ?>/"+awal+"/"+akhir);
            table.ajax.reload();  //just reload table
        });
    });

</script>