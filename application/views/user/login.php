<form name="form1" action="<?php echo $form_action; ?>" method="POST">
<div class="login">
    <!-- Login -->
    <div class="login__block active" id="l-login">
        <div class="login__block__header">
            <i class="zmdi zmdi-account-circle"></i>
            Pesantren Enterprise! Silahkan login
            <div class="actions actions--inverse login__block__actions">
                <div class="dropdown">
                    <i data-toggle="dropdown" class="zmdi zmdi-more-vert actions__item"></i>
                    <div class="dropdown-menu dropdown-menu-right">
                        <!-- <a class="dropdown-item" data-ma-action="login-switch" data-ma-target="#l-register" href="">Registrasi</a> -->
                        <a class="dropdown-item" data-ma-action="login-switch" data-ma-target="#l-forget-password" href="">Lupa Password?</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="login__block__body">
            <?php echo $this->alert->disp_sys_msg(); ?>
                <div class="form-group form-group--float form-group--centered">
                    <input type="text" name="u_name" id="u_name" class="form-control" required="">
                    <label>Username</label>
                    <i class="form-group__bar"></i>
                </div>
                <div class="form-group form-group--float form-group--centered">
                    <input type="password" name="u_pass" id="u_pass" class="form-control">
                    <label>Password</label>
                    <i class="form-group__bar"></i>
                </div>
                <button type="submit" name="btnLogin" id="btnLogin" value="btnLogin" class="btn btn--icon login__block__btn"><i class="zmdi zmdi-long-arrow-right"></i></button>
            
        </div>
    </div>
    <!-- Register -->
    <!--
    <div class="login__block" id="l-register">
        <div class="login__block__header palette-Blue bg">
            <i class="zmdi zmdi-account-circle"></i>
            Create an account
            <div class="actions actions--inverse login__block__actions">
                <div class="dropdown">
                    <i data-toggle="dropdown" class="zmdi zmdi-more-vert actions__item"></i>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" data-ma-action="login-switch" data-ma-target="#l-login" href="">Already have an account?</a>
                        <a class="dropdown-item" data-ma-action="login-switch" data-ma-target="#l-forget-password" href="">Forgot password?</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="login__block__body">
            <div class="form-group form-group--float form-group--centered">
                <input type="text" class="form-control">
                <label>Name</label>
                <i class="form-group__bar"></i>
            </div>
            <div class="form-group form-group--float form-group--centered">
                <input type="text" class="form-control">
                <label>Email Address</label>
                <i class="form-group__bar"></i>
            </div>
            <div class="form-group form-group--float form-group--centered">
                <input type="password" class="form-control">
                <label>Password</label>
                <i class="form-group__bar"></i>
            </div>
            <div class="form-group">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Accept the license agreement</span>
                </label>
            </div>
            <a href="<?php echo base_url() . 'assets/Bootstrap4/' ?>index.html" class="btn btn--icon login__block__btn"><i class="zmdi zmdi-plus"></i></a>
        </div>
    </div>
    -->
    <!-- Forgot Password -->
    <div class="login__block" id="l-forget-password">
        <div class="login__block__header palette-Purple bg">
            <i class="zmdi zmdi-account-circle"></i>
            Lupa Password?
            <div class="actions actions--inverse login__block__actions">
                <div class="dropdown">
                    <i data-toggle="dropdown" class="zmdi zmdi-more-vert actions__item"></i>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" data-ma-action="login-switch" data-ma-target="#l-login" href="">Back to Login</a>
                        <!-- <a class="dropdown-item" data-ma-action="login-switch" data-ma-target="#l-register" href="">Register</a> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="login__block__body">
            <p class="mt-4">Silahkan masukkan alamat email anda. Link untuk melakukan reset password akan dikirimkan ke email anda.</p>
            <div class="form-group form-group--float form-group--centered">
                <input type="text" class="form-control">
                <label>Email </label>
                <i class="form-group__bar"></i>
            </div>
            <button type="submit" name="btnReset" id="btnReset" value="btnReset" class="btn btn--icon login__block__btn"><i class="zmdi zmdi-checkd"></i></button>
        </div>
    </div>
</div>
</form>