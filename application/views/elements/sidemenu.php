<div class="scrollbar-inner">
    <div class="user">
        <div class="user__info" data-toggle="dropdown">
            <img class="user__img" src="<?php echo base_url() . 'assets/Bootstrap4/' ?>demo/img/profile-pics/8.jpg" alt="">
            <div>
                <div class="user__name"><?php echo $this->session->username; ?></div>
                <div class="user__email"><?php echo $this->session->email; ?></div>
            </div>
        </div>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="<?php echo base_url() ?>user/profile">View Profile</a>
            <a class="dropdown-item" href="<?php echo base_url() ?>user/setting">Settings</a>
            <a class="dropdown-item" href="<?php echo base_url() ?>user/logout">Logout</a>
        </div>
    </div>
    <ul class="navigation">
        <li><a href="<?php echo base_url() ?>home/dashboard"><i class="zmdi zmdi-home"></i> Beranda</a></li>
        <li class="navigation__sub navigation__sub--toggled">
            <a href=""><i class="zmdi zmdi-account-box"></i> Registrasi</a>
            <ul>
                <li><a href="<?php echo base_url() ?>registrasi/register_kyai">Registrasi Kyai</a></li>
                <li><a href="<?php echo base_url() ?>registrasi/register_ustadz">Registrasi Ustadz</a></li>
                <li><a href="<?php echo base_url() ?>registrasi/register_jamaah">Registrasi Jamaah</a></li>
                <li><a href="<?php echo base_url() ?>registrasi/aktivasi">Aktivasi Anggota</a></li>
                <li><a href="<?php echo base_url() ?>registrasi/index">Data Anggota</a></li>
            </ul>
        </li>
        <li class="navigation__sub">
            <a href=""><i class="zmdi zmdi-view-list"></i> Pembayaran</a>
            <ul>
                <li><a href="<?php echo base_url() ?>pembayaran/angsuran_add">Angsuran Anggota</a></li>
                <li><a href="<?php echo base_url() ?>pembayaran/angsuran_konfirmasi">Konfirmasi Angsuran Anggota</a></li>
                <li><a href="<?php echo base_url() ?>pembayaran/fee_konfirmasi">Konfirmasi Fee Anggota</a></li>
            </ul>
        </li>
        <li class="navigation__sub">
            <a href=""><i class="zmdi zmdi-collection-text"></i> Laporan</a>
            <ul>
                <li><a href="<?php echo base_url() ?>registrasi/lap_registrasi">Laporan Registrasi</a></li>
                <li><a href="<?php echo base_url() ?>pembayaran/lap_angsuran">Laporan Angsuran Anggota</a></li>
                <li><a href="<?php echo base_url() ?>pembayaran/lap_fee_anggota">Laporan Fee Anggota</a></li>
                <!-- <li><a href="<?php echo base_url() ?>pembayaran/jurnal_anggota">Jurnal Anggota</a></li> -->
                <li><a href="<?php echo base_url() ?>pembayaran/jurnal_harian">Jurnal Harian</a></li>
            </ul>
        </li>
        <li class="navigation__sub">
            <a href=""><i class="zmdi zmdi-view-week"></i> Administrator</a>
            <ul>
                <li><a href="<?php echo base_url() ?>administrator/setting">Setting</a></li>
                <li><a href="<?php echo base_url() ?>administrator/pengguna">Pengelolaan Pengguna</a></li>
                <li><a href="<?php echo base_url() ?>administrator/hak_akses">Pengelolaan Hak Akses</a></li>
                <li><a href="<?php echo base_url() ?>administrator/laporan_aktivitas">Laporan Aktivitas</a></li>
                <li><a href="<?php echo base_url() ?>administrator/laporan_error">Laporan Error</a></li>
            </ul>
        </li>
    </ul>
</div>