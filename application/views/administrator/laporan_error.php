<header class="content__title">
    <h1>Laporan Error</h1>
    <div class="actions">
        <a href="" class="actions__item zmdi zmdi-trending-up"></a>
        <a href="" class="actions__item zmdi zmdi-check-all"></a>
        <div class="dropdown actions__item">
            <i data-toggle="dropdown" class="zmdi zmdi-more-vert"></i>
            <div class="dropdown-menu dropdown-menu-right">
                <a href="" class="dropdown-item">Refresh</a>
                <a href="" class="dropdown-item">Manage Widgets</a>
                <a href="" class="dropdown-item">Settings</a>
            </div>
        </div>
    </div>
</header>

<div class="card">
    <div class="card-header">
        <h2 class="card-title">Laporan Error</h2>
        <small class="card-subtitle">Data error</small>
    </div>

    <div class="card-block">
        <form method="POST" id="form_aktivasi" name="form_aktivasi">
            <div class="form-group">
                <label>TEST</label>
                <textarea class="form-control" rows="10"><?php print_r(json_decode($error['konten'])); ?></textarea>>
                <i class="form-group__bar"></i>
            </div>
        </form>
        <div class="modal fade" id="modal-default" tabindex="-1">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title pull-left">Detail Error</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>IP Address</label>
                                    <input type="text" name="ip_address" id="ip_address" class="form-control" readonly="">
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" name="username" id="username" class="form-control" readonly="">
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Aktivitas</label>
                                    <input type="text" name="aktivitas" id="aktivitas" class="form-control" readonly="">
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Konten/Data</label>
                                    <textarea id="konten" name="konten" rows="6" class="form-control" readonly=""></textarea>
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Error Message</label>
                                    <textarea id="error_message" name="error_message" rows="6" class="form-control" readonly=""></textarea>
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="table-responsive">
            <table id="example1" class="table table-bordered table-condensed table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>IP Address</th>
                        <th>Username</th>
                        <th>Aktivitas</th>
                        <th>Error Message</th>
                        <th>Data</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Vendors -->
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/tether/dist/js/tether.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/Waves/dist/waves.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>

<!-- Vendors: Data tables -->
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jszip/dist/jszip.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>


<script type="text/javascript">
    var table;
    $(document).ready(function () {
        table = $('#example1').DataTable({
            "ajax": {
                "url": "<?php echo site_url('administrator/ajax_laporan_error') ?>",
            },
            "columnDefs": [
                {
                    "targets": [0], 
                    "orderable": false, 
                },
            ],
        });
    });
    function detail(log_error_id) {
        var link_rm = "<?php echo site_url('administrator/get_data_error') ?>";
        link_rm = link_rm + "/" + log_error_id;
        $.get(link_rm, function (data) {
            $('#ip_address').val(data.ip_address);
            $('#username').val(data.username);
            $('#aktivitas').val(data.aktivitas);
            $('#error_message').val(data.error_message);
            $('#konten').val(data.konten);
        }, "json");
        $('#modal-default').modal('show');
    };
</script>