<div class="content__inner">
    <header class="content__title">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>/registrasi">Administrator</a></li>
            <li class="breadcrumb-item active">Pengelolaan Hak Akses</li>
        </ol>
    </header>
    <form method="POST">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Hak Akses Kelompok</h2>
            </div>
            <div class="card-block">
                <div class="form-group form-group--float" style="margin-top:0px;">
                    <input type="text" name="nama_lengkap" id="nama_lengkap" class="form-control" value="<?php echo $kelompok['nama']; ?>" required="">
                    <label>Nama Kelompok</label>
                    <i class="form-group__bar"></i>
                </div>
                <div class="form-group">
                    <table class="table table-condensed table-responsive">
                        <thead>
                            <tr>
                                <th>Modul</th>
                                <th>Menu</th>
                                <th>View</th>
                                <th>Add</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list_menu as $item) : ?>
                            <tr>
                                <td>
                                    <input type="hidden" id="menu_id[]" name="menu_id[]" value="<?php echo $item['id']; ?>">
                                    <?php echo $item['modul']; ?>
                                </td>
                                <td><?php echo $item['action']; ?></td>
                                <td class="text-center">
                                    <?php $is_checked = !empty($item['view'])? 'checked' : ''; ?>
                                    <input type="hidden" class="checkbox_handler" name="view[]" value="<?php echo !empty($is_checked) ? '1':'0'; ?>"  />
                                    <input type="checkbox" name="view_ck[]" value="1" <?php echo $is_checked; ?> />
                                </td>
                                <td class="text-center">
                                    <?php $is_checked = !empty($item['add'])? 'checked' : ''; ?>
                                    <input type="hidden" class="checkbox_handler" name="add[]" value="<?php echo !empty($is_checked) ? '1':'0'; ?>"  />
                                    <input type="checkbox" name="add_ck[]" value="1" <?php echo $is_checked; ?> />
                                </td>
                                <td class="text-center">
                                    <?php $is_checked = !empty($item['edit'])? 'checked' : ''; ?>
                                    <input type="hidden" class="checkbox_handler" name="edit[]" value="<?php echo !empty($is_checked) ? '1':'0'; ?>"  />
                                    <input type="checkbox" name="edit_ck[]" value="1" <?php echo $is_checked; ?> />
                                </td>
                                <td class="text-center">
                                    <?php $is_checked = !empty($item['delete'])? 'checked' : ''; ?>
                                    <input type="hidden" class="checkbox_handler" name="delete[]" value="<?php echo !empty($is_checked) ? '1':'0'; ?>"  />
                                    <input type="checkbox" name="delete_ck[]" value="1" <?php echo $is_checked; ?> />
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                
                <button type="submit" class="btn btn-lg btn-primary">Simpan</button>                
            </div>
        </div>
    </form>
</div>
<script>
$(document).on("change", "input[type='checkbox']", function() {
    var checkbox_val = ( this.checked ) ? 1 : 0;
    $(this).siblings('input.checkbox_handler').val(checkbox_val);
});
</script>