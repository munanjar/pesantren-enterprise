<header class="content__title">
    <h1>Data Pengguna</h1>

    <div class="actions">
        <a href="" class="actions__item zmdi zmdi-trending-up"></a>
        <a href="" class="actions__item zmdi zmdi-check-all"></a>

        <div class="dropdown actions__item">
            <i data-toggle="dropdown" class="zmdi zmdi-more-vert"></i>
            <div class="dropdown-menu dropdown-menu-right">
                <a href="" class="dropdown-item">Refresh</a>
                <a href="" class="dropdown-item">Manage Widgets</a>
                <a href="" class="dropdown-item">Settings</a>
            </div>
        </div>
    </div>
</header>

<div class="card">
    <div class="card-header">
        <h2 class="card-title">Data Pengguna </h2>
        <small class="card-subtitle">List Data Pengguna Pesantren Enterprise</small>
    </div>

    <div class="card-block">
        <button class="btn btn-primary" onclick="add_user()">Add User</button>
        <div class="modal fade" id="modal-default" data-backdrop="false" tabindex="-1">
            <div class="modal-dialog">
                <form method="POST" id="form_aktivasi" name="form_detail">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title pull-left">Data Pengguna</h5>
                        </div>
                        <div class="modal-body">
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label style="color: red"><b>Username</b></label>
                                            <input type="hidden" name="id" id="id">
                                            <input type="text" name="username" id="username" class="form-control" required="" placeholder="Username">

                                            <i class="form-group__bar"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-12"></div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label style="color: red"><b>Password</b></label>&nbsp;&nbsp;&nbsp;
                                            <label id="lreset"><input type="checkbox" id="reset" name="reset"> Reset Password</label>
                                            <input type="password" name="pass" id="pass" class="form-control" required="" placeholder="Password">
                                            <i class="form-group__bar"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-12"></div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label style="color: red"><b>Nama Pengguna</b></label>
                                            <div class="form-group">
                                                <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama Pengguna">
                                                <i class="form-group__bar"></i>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label style="color: red"><b>Kelompok</b></label>
                                            <div class="form-group">
                                                <select class="form-control" id="kelompok" name="kelompok">
                                                    <option>Kelompok</option>
                                                </select>
                                                
                                                <i class="form-group__bar"></i>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label style="color: red"><b>Status</b></label>
                                            <div class="form-group">
                                                <select class="form-control" id="status" name="status">
                                                    <option>Status</option>
                                                    <option value="1">Aktif</option>
                                                    <option value="0">Non Aktif</option>
                                                </select>
                                                
                                                <i class="form-group__bar"></i>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button id="btnsave" type="submit" class="btn btn-lg btn-primary" onclick="update_data()">Simpan</button>
                            <button type="button" class="btn btn-lg btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table id="example1" class="table table-bordered table-condensed table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Username</th>
                        <th>Nama Pengguna</th>
                        <th>Kelompok</th>
                        <th>Modified By</th>
                        <th>Modified Date</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Vendors -->
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/tether/dist/js/tether.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/Waves/dist/waves.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>



<script type="text/javascript">
    var table;
    $(document).ready(function () {
        table = $('#example1').DataTable({
            "ajax": {
                "url": "<?php echo site_url('Administrator/ajax_data_pengguna') ?>",
            },
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],
        });
    });
    
    function add_user(){
        $('#id').val('');
        $('#username').val('');
        $('#pass').val('');
        $('#nama').val('');
        $('#kelompok').val(''); 
        $('#status').val(''); 
        
        document.getElementById("lreset").style.visibility = "hidden";
        $('#modal-default').modal('show');
    }
    
    function edit_data(id) {
        
        var link_rm = "<?php echo site_url('Administrator/edit') ?>";
        link_rm = link_rm + "/" + id;
        $.get(link_rm, function (data) {
            $('#id').val(data.id);
            $('#username').val(data.username);
            $('#pass').val(data.password);
            $('#nama').val(data.nama);
            $('#kelompok').val(data.kelompok); 
            $('#status').val(data.status); 

        }, "json");

        $('#modal-default').modal('show');
        
    };

    function delete_data(id)
    {
        if (confirm('Apakah Anda Akan Menghapus Data Ini (' + id + ') '))
        {
            $.ajax({
                url: "<?php echo site_url('Administrator/delete_user') ?>/" + id,
                type: "POST",
                dataType: "JSON",
                success: function (data)
                {
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    table.ajax.reload();
                }
            });
        }
    }

    function simpan_aktivasi() {
        $('#form_aktivasi').submit();
    };
</script>

<script type='text/javascript' src='<?php echo base_url(); ?>assets/jquery.autocomplete.js'></script>
<link href='<?php echo base_url(); ?>assets/jquery.autocomplete.css' rel='stylesheet' />

<script type='text/javascript'>
    var site = "<?php echo base_url(); ?>";
    $(function () {
        $('.autocomplete').autocomplete({
            serviceUrl: site + 'registrasi/search_ustadz',
            onSelect: function (suggestion) {
                $('#ustadz_id').val('' + suggestion.anggota_id);
            }
        });

        $('#provinsi').change(function () {
            var url = "<?php echo base_url('registrasi/add_ajax_kab') ?>/" + $(this).val();
            $('#kabupaten').load(url);
            //return false;
        });
        
        $('#reset').click(function () {
            var cek = $('#reset').val();
            if(cek === 'on'){
                $('#pass').val('');
            }
        });
    });

    $(document).ready(function () {
        var url = "<?php echo base_url('Administrator/add_ajax_kelompok') ?>";
        $("#kelompok").load(url);
       
    });
    
    

</script>