<header class="content__title">
    <header class="content__title">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="">Administrator</a></li>
            <li class="breadcrumb-item active">Pengelolaan Hak Akses</li>
        </ol>
    </header>
</header>

<div class="card">
    <div class="card-header">
        <h2 class="card-title">Pengelolaan Hak Akses </h2>
        <small class="card-subtitle">Daftar Kelompok User dan Hak Aksesnya</small>
    </div>

    <div class="card-block">
        <button type="button" class="text-center btn btn-small btn-primary" onclick="add_kelompok()">BARU</button>
        <div class="modal fade" id="modal-default" tabindex="-1">
            <div class="modal-dialog">
                <form method="POST" id="form_kelompok" name="form_kelompok">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title pull-left">Kelompok User</h5>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Kelompok</label>
                                <input type="hidden" name="aktivasi_id" id="aktivasi_id" class="form-control" readonly="">
                                <input type="text" name="nama_kelompok" id="nama_kelompok" class="form-control" readonly="">
                                <i class="form-group__bar"></i>
                            </div>                            
                            <div class="form-group">
                                <label>Keterangan</label>
                                <input type="text" name="deskripsi" id="deskripsi" class="form-control" readonly="">
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link" onclick="simpan_kelompok()">Simpan</button>
                            <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table id="example1" class="table table-bordered table-condensed table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kelompok</th>
                        <th>Deskripsi</th>
                        <th>Aksi</th>
                        <th>Hak Akses</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Vendors -->
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/tether/dist/js/tether.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/Waves/dist/waves.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>

<!-- Vendors: Data tables -->
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/jszip/dist/jszip.min.js"></script>
<script src="<?php echo base_url() . 'assets/Bootstrap4/' ?>vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>


<script type="text/javascript">
    var table;
    $(document).ready(function () {
        table = $('#example1').DataTable({
            "ajax": {
                "url": "<?php echo site_url('administrator/ajax_data_kelompok') ?>",
            },
            "columnDefs": [
                {
                    "targets": [0], 
                    "orderable": false, 
                },
            ],
        });
    });
    function kelompok(kelompok_id) {
        var link_rm = "<?php echo site_url('administrator/get_data_kelompok') ?>";
        link_rm = link_rm + "/" + kelompok_id;
        $.get(link_rm, function (data) {
            $('#kelompok_id').val(data.kelompok_id);
            $('#nama_kelompok').val(data.nama);
            $('#deskripsi').val(data.keterangan);
        }, "json");
        $('#modal-default').modal('show');
    };
    function add_kelompok() {
        $('#kelompok_id').val('');
        $('#nama_kelompok').val('');
        $('#deskripsi').val('');
        $('#modal-default').modal('show');
    };
    function simpan_kelompok() {
        $('#form_kelompok').submit();
    };
</script>