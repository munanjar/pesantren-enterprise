<header class="content__title">
    <h1>Dashboard</h1>
    <small>Selamat Datang di halaman Dashboard khusus Administrator!</small>

    <div class="actions">
        <a href="" class="actions__item zmdi zmdi-trending-up"></a>
        <a href="" class="actions__item zmdi zmdi-check-all"></a>

        <div class="dropdown actions__item">
            <i data-toggle="dropdown" class="zmdi zmdi-more-vert"></i>
            <div class="dropdown-menu dropdown-menu-right">
                <a href="" class="dropdown-item">Refresh</a>
                <a href="" class="dropdown-item">Manage Widgets</a>
                <a href="" class="dropdown-item">Settings</a>
            </div>
        </div>
    </div>
</header>

<div class="row quick-stats">
    <div class="col-sm-6 col-md-3">
        <div class="quick-stats__item bg-light-blue">
            <div class="quick-stats__info">
                <h2><?php echo number_format($total_anggota,0,',','.'); ?></h2>
                <small>Total Anggota</small>
            </div>

            <div class="quick-stats__chart sparkline-bar-stats">6,4,8,6,5,6,7,8,3,5,9,5</div>
        </div>
    </div>

    <div class="col-sm-6 col-md-3">
        <div class="quick-stats__item bg-amber">
            <div class="quick-stats__info">
                <h2><?php echo number_format($registrasi_bulan_berjalan,0,',','.'); ?></h2>
                <small>Registrasi Bulan Ini</small>
            </div>

            <div class="quick-stats__chart sparkline-bar-stats">4,7,6,2,5,3,8,6,6,4,8,6</div>
        </div>
    </div>

    <div class="col-sm-6 col-md-3">
        <div class="quick-stats__item bg-purple">
            <div class="quick-stats__info">
                <h2>Rp. <?php echo number_format($total_fee_anggota,0,',','.'); ?></h2>
                <small>Total Fee Anggota</small>
            </div>

            <div class="quick-stats__chart sparkline-bar-stats">9,4,6,5,6,4,5,7,9,3,6,5</div>
        </div>
    </div>

    <div class="col-sm-6 col-md-3">
        <div class="quick-stats__item bg-red">
            <div class="quick-stats__info">
                <h2>Rp. <?php echo number_format($total_angsuran,0,',','.'); ?></h2>
                <small>Total Angsuran</small>
            </div>

            <div class="quick-stats__chart sparkline-bar-stats">5,6,3,9,7,5,4,6,5,6,4,9</div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6 col-md-4">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Komposisi Anggota</h2>
            </div>

            <div class="card-block">
                <div class="flot-chart flot-pie"></div>
                <div class="flot-chart-legends flot-chart-legend--pie"></div>
            </div>
        </div>
        <div class="card-demo">
            <div class="card card-outline-primary">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th></th>
                            <th>Bulan</th>
                            <th>Tahun</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Registrasi</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>Aktivasi</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>Fee Anggota</td>
                            <td>  </td>
                            <td></td>
                        </tr>
                        <tr>
                            <th scope="row">4</th>
                            <td>Angsuran Anggota</td>
                            <td>  </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Registrasi Anggota</h2>
            </div>

            <div class="card-block">
                <div class="flot-chart flot-line"></div>
                <div class="flot-chart-legends flot-chart-legends--line"></div>
            </div>
        </div>
        
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Transaksi</h2>
            </div>

            <div class="card-block">
                <div class="flot-chart flot-bar"></div>
                <div class="flot-chart-legends flot-chart-legends--bar"></div>
            </div>
        </div>
    </div>
</div>