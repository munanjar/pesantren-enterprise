<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    //set the class variable.
    public $template = array();
    public $data = array();

    // Loading the default libraries, helper, language 
    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'language', 'url'));
        $this->load->library('session');
        $this->load->model('log_aktivitas');
        $this->load->model('log_error');
    }

    // Front Page Layout -- TEST
    public function layout() {
        // making template and send data to view.
        $this->template['header'] = $this->load->view('layout/header', $this->data, true);
        $this->template['left'] = $this->load->view('layout/left', $this->data, true);
        $this->template['middle'] = $this->load->view($this->middle, $this->data, true);
        $this->template['footer'] = $this->load->view('layout/footer', $this->data, true);
        $this->load->view('layout/front', $this->template);
    }
    
    function myDebug($data){
        echo '<pre>';
        print_r($data);
        die();
    }
    
    function get_current_username(){
        $nama = $this->session->userdata('username');
        return $nama;
    }
    
    function simpan_aktivitas($aktivitas, $konten) {
        $log_aktivitas = array(
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'username' => $this->session->userdata('username'),
            'aktivitas' => $aktivitas,
            'konten' => json_encode($konten),
            'created' => date('Y-m-d H:i:s')
        );
        $simpan = $this->log_aktivitas->save_aktivitas($log_aktivitas);
    }

    function simpan_error($aktivitas, $konten, $error_message) {
        $log_error = array(
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'username' => $this->session->userdata('username'),
            'aktivitas' => $aktivitas,
            'konten' => json_encode($konten),
            'error_message' => $aktivitas,
            'created' => date('Y-m-d H:i:s')
        );
        $simpan = $this->log_error->save_error($log_error);
    }
    
    function get_number_from_string($string){
        return preg_replace("/[^0-9]/","",$string);
    }

}
?>
